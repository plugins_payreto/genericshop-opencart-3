<?php
/* Generic Shop general model
 *
 * @version 3.0.0
 * @date 2018-04-25
 *
 */
class ModelExtensionPaymentGenericshopAgeneral extends Model {

	/**
	* Get the Method
	* this funtion is the OpenCart funtion
	*
	* @param string $address
	* @param int $total
	* @return  boolean
	*/
	public function getMethod($address, $total) {
		return false;
	}

	/**
	* Check if the payment method not allowed in backend
	*
	* @return  boolean
	*/
	public function isNotAllowedPaymentBackend() {
		return true;
	}
}
