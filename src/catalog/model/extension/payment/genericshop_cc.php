<?php
/* Generic Shop Credit Cards model
 *
 * @version 3.0.0
 * @date 2018-04-25
 *
 */
include_once(dirname(__FILE__) . '/../../genericshop/genericshop.php');

class ModelExtensionPaymentGenericshopCc extends ModelGenericshopGenericshop {
	protected $code = 'genericshop_cc';
	protected $title = 'FRONTEND_PM_CC';

	/**
	 * get the payment method logo
	 *
	 * @return string
	 */
	public function getLogo() {
		$this->brand = $this->config->get('payment_' . $this->code . '_cards_types');
		return $this->getCardsLogo();
	}
}
