<?php
/* Generic Shop Credit Cards (recurring) model
 *
 * @version 3.0.0
 * @date 2018-04-25
 *
 */
include_once(dirname(__FILE__) . '/../../genericshop/genericshop.php');

class ModelExtensionPaymentGenericshopCcSaved extends ModelGenericshopGenericshop {
	protected $code = 'genericshop_cc_saved';
	protected $title = 'FRONTEND_MC_PM_CARDSSAVED';
	protected $group_recurring = 'CC';

	/**
	 * get the payment method logo
	 *
	 * @return string
	 */
	public function getLogo() {
		$this->brand = $this->config->get('payment_' . $this->code . '_cards_types');
		return $this->getCardsLogo();
	}
}
