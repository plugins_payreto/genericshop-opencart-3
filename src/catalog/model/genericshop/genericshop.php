<?php
/* Generic Shop payment method model
 *
 * @version 3.0.0
 * @date 2018-04-25
 *
 */
include_once(dirname(__FILE__) . '/../../controller/genericshop/core.php');
include_once(dirname(__FILE__) . '/../../controller/genericshop/genericshop.php');

class ModelGenericshopGenericshop extends Model
{
	protected $code = '';
	protected $title = '';
	protected $group_recurring = '';
	protected $logo = '';
	protected $is_logo_depend_on_language = false;
	protected $brand = '';

	/**
	 * Get the Method
	 * this funtion is the OpenCart funtion
	 *
	 * @param string $address
	 * @param int $total
	 * @return  array
	 */
	public function getMethod($address, $total) {
		$this->language->load('extension/payment/genericshop');
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('payment_' . $this->code . '_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if ($this->config->get('payment_' . $this->code . '_total') > 0 && $this->config->get('payment_' . $this->code . '_total') > $total) {
			$status = false;
		} elseif (!$this->config->get('payment_' . $this->code . '_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}
		if ($this->code == 'genericshop_cc_saved') {
			if ($this->config->get('payment_genericshop_ageneral_recurring') == '0') {
				$status = false;
			}
			if (!isset($this->session->data['customer_id'])) {
				$status = false;
			}
		}
		if ($this->code == 'genericshop_cc') {
			if ($this->config->get('payment_genericshop_ageneral_recurring') == '1') {
				$status = false;
			}
			if (!isset($this->session->data['customer_id'])) {
				$status = true;
			}
		}
		if ($this->code == 'genericshop_cc' || $this->code == 'genericshop_cc_saved') {
			$cards_types = trim($this->config->get('payment_' . $this->code . '_cards_types'));
			if (empty($cards_types)) {
				$status = false;
			}
		}
		$method_data = array();
		if ($status) {
			$method_data = array(
				'code'       	=> $this->code,
				'title'      	=> $this->language->get($this->title),
				'logo'			=> $this->getLogo(),
				'terms'		 	=> '',
				'sort_order' 	=> $this->config->get('payment_' . $this->code . '_sort_order')
			);
		}
		return $method_data;
	}

	/**
	 * get the payment method logo
	 *
	 * @return string
	 */
	public function getLogo() {
		if ($this->is_logo_depend_on_language && substr($this->session->data['language'], 0, 2) == 'de') {
			$this->logo = str_replace('_en', '_de', $this->logo);
		}

		if (file_exists('catalog/view/theme/' . $this->config->get('config_template') . '/image/genericshop/' . $this->logo)) {
			$logo_html = '<img src="catalog/view/theme/' . $this->config->get('config_template') . '/image/genericshop/' . $this->logo . '" border="0" style="height:35px;">';
		} else {
			$logo_html = '<img src="catalog/view/theme/default/image/genericshop/' . $this->logo . '" border="0" style="height:35px;">';
		}
		return $logo_html;
	}

	/**
	 * get cards logo
	 *
	 * @return string
	 */
	public function getCardsLogo() {
		$logo_html = '';
		$cards = explode(',', $this->brand);
		if (is_array($cards)) {
			foreach ($cards as $value) {
				$logo = strtolower($value) . '.png';
				if (file_exists('catalog/view/theme/' . $this->config->get('config_template') . '/image/genericshop/' . $logo)) {
					$logo_html .= '<img src="catalog/view/theme/' . $this->config->get('config_template') . '/image/genericshop/' . $logo . '"" alt=' . $value . ' border="0" style="margin-right:5px; height:35px;">';
				} else {
					$logo_html .= '<img src="catalog/view/theme/default/image/genericshop/' . $logo . '"" alt=' . $value . ' border="0" style="margin-right:5px; height:35px;">';
				}
			}
		}
		return $logo_html;
	}

	/**
	 * Check if the payment method not allowed in backend
	 *
	 * @return  boolean
	 */
	public function isNotAllowedPaymentBackend() {
		if ($this->config->get('payment_genericshop_ageneral_recurring')) {
			if ($this->code == 'genericshop_cc_saved' && $this->getPaymentAccount('genericshop_cc_saved')) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Save the Generic Shop Order data into the database
	 *
	 * @param array $data
	 * @return  void
	 */
	public function saveOrder($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "payment_genericshop_orders` (orders_id, unique_id, amount, currency, payment_type, payment_method) VALUES ('" . (int)$data['orders_id'] . "', '" . $data['unique_id'] . "', " . $data['amount'] . ", '" . $data['currency'] . "', '" . $data['payment_type'] . "', '" . $this->db->escape($data['payment_method']) . "')");
	}

	/**
	 * check the payment account if its has default setting into the database
	 *
	 * @param string $payment_method
	 * @return  void
	 */
	public function checkDefault($payment_method) {
		$credentials = $this->getCredentials($payment_method);
		$group_recurring = $this->getGroupRecurring($payment_method);
		$query = $this->db->query("select * from " . DB_PREFIX . "payment_genericshop_recurring where cust_id = '" . (int)$this->customer->getId() . "' and server_mode = '" . $credentials['server_mode'] . "' and channel_id = '" . $credentials['channel_id'] . "' and payment_group = '" . $group_recurring . "' and payment_default = '1'");
		return $query->num_rows;
	}

	/**
	 * Set default setting for the payment account.
	 *
	 * @param string $id
	 * @param string $group_recurring
	 * @return  void
	 */
	public function setDefault($id, $group_recurring) {
		$this->db->query("UPDATE " . DB_PREFIX . "payment_genericshop_recurring SET payment_default = '0' WHERE payment_default = '1' AND payment_group ='" . $this->db->escape($group_recurring) . "'");
		$this->db->query("UPDATE " . DB_PREFIX . "payment_genericshop_recurring SET payment_default = '1' WHERE id = '" . (int)$id . "' AND payment_group ='" . $this->db->escape($group_recurring) . "'");
	}

	/**
	 * Get a customer data
	 *
	 * @return  array
	 */
	public function getCustomerData() {
		$query = $this->db->query("select * from " . DB_PREFIX . "customer where customer_id = '" . (int)$this->customer->getId() . "'");
		return $query->rows;
	}

	/**
	 * Get a customer address
	 *
	 * @return  array
	 */
	public function getCustomerAddress() {
		$query = $this->db->query("select * from " . DB_PREFIX . "address where customer_id = '" . (int)$this->customer->getId() . "'");
		return $query->rows;
	}

	/**
	 * Get a country code based on country id
	 *
	 * @param   string  $country_id
	 * @return  string
	 */
	public function getCountryCode($country_id) {
		$query = $this->db->query("select * from " . DB_PREFIX . "country where country_id = '" . (int)$country_id . "'");
		return $query->row['iso_code_2'];
	}

	/**
	 * Insert the payment account data into the database
	 *
	 * @param array $data
	 * @return  void
	 */
	public function savePaymentAccount($data) {
		$this->db->query("insert into " . DB_PREFIX . "payment_genericshop_recurring (cust_id, payment_group, brand, email, holder, last4digits, expiry_month, expiry_year, server_mode, channel_id, ref_id, payment_default) values ('" . (int)$data['customer_id'] . "', '" . $this->db->escape($data['group_recurring']) . "', '" . $data['payment_brand'] . "', '" . $data['email'] . "', '" . $data['holder'] . "', '" . $data['last4Digits'] . "', '" . $data['expiryMonth'] . "', '" . $data['expiryYear'] . "', '" . $data['server_mode'] . "', '" . $data['channel_id'] . "', '" . $data['referenceId'] . "', '" . $data['default'] . "')");
	}

	/**
	 * Update the payment account data into the database
	 *
	 * @param array $data
	 * @return  void
	 */
	public function updatePaymentAccount($data) {
		$this->db->query("update " . DB_PREFIX . "payment_genericshop_recurring set cust_id ='" . (int)$data['customer_id'] . "', payment_group = '" . $this->db->escape($data['group_recurring']) . "', brand = '" . $data['payment_brand'] . "', email = '" . $data['email'] . "', holder = '" . $data['holder'] . "', last4digits = '" . $data['last4Digits'] . "', expiry_month = '" . $data['expiryMonth'] . "', expiry_year = '" . $data['expiryYear'] . "', server_mode = '" . $data['server_mode'] . "', channel_id = '" . $data['channel_id'] . "', ref_id = '" . $data['referenceId'] . "'  where id = '" . (int)$data['recurring_id'] . "'");
	}

	/**
	 * Delete the payment account
	 *
	 * @param string $recurring_id
	 * @return  void
	 */
	public function deletePaymentAccount($recurring_id) {
		$this->db->query("delete from " . DB_PREFIX . "payment_genericshop_recurring where cust_id = '" . (int)$this->customer->getId() . "' and id = '" . (int)$recurring_id . "'");
	}

	/**
	 * Check availability the reference id
	 *
	 * @param   string  $reference_id
	 * @return  string
	 */
	public function checkReferenceId($reference_id) {
		$query = $this->db->query("select ref_id from " . DB_PREFIX . "payment_genericshop_recurring where cust_id = '" . (int)$this->customer->getId() . "' and ref_id='" . $this->db->escape($reference_id) . "'");
		return $query->num_rows;
	}

	/**
	 * Get a reference id
	 *
	 * @param   string  $recurring_id
	 * @return  string
	 */
	public function getReferenceId($recurring_id) {
		$query = $this->db->query("select ref_id from " . DB_PREFIX . "payment_genericshop_recurring where cust_id = '" . (int)$this->customer->getId() . "' and id='" . (int)$recurring_id . "'");
		return $query->row['ref_id'];
	}

	/**
	 * Get the group of payment methods recurring
	 *
	 * @param   string  $payment_method
	 * @return  string
	 */
	function getGroupRecurring($payment_method) {
		switch ($payment_method) {
			case 'genericshop_cc_saved':
				return 'CC';
				break;
		}
		return false;
	}

	/**
	 * Get payment accounts list
	 *
	 * @param   string  $payment_method
	 * @return  array
	 */
	public function getPaymentAccount($payment_method) {
		$credentials = $this->getCredentials($payment_method);
		$group_recurring = $this->getGroupRecurring($payment_method);
		$query = $this->db->query("select * from " . DB_PREFIX . "payment_genericshop_recurring where cust_id = '" . (int)$this->customer->getId() . "' and server_mode = '" . $credentials['server_mode'] . "' and channel_id = '" . $credentials['channel_id'] . "' and payment_group = '" . $group_recurring . "'");
		return $query->rows;
	}

	/**
	 * Get payment accounts list in backend order
	 *
	 * @return  array
	 */
	public function getBackendRecurringLists() {
		$recurring_lists = $this->getPaymentAccount($this->code);
		$check_default = $this->checkDefault($this->code);
		if ($check_default <= 0) {
			if (is_array($recurring_lists))
				$recurring_lists[0]['payment_default'] = 1;
		}
		return $recurring_lists;
	}

	/**
	 * Get order status failed
	 *
	 * @return  string
	 */
	public function getOrderStatusFailed() {
		$query = $this->db->query("SELECT order_status_id FROM " . DB_PREFIX . "order_status WHERE name = 'failed' limit 1");
		return $query->row['order_status_id'];
	}

	/**
	 * update payment status
	 *
	 * @param   string  $status
	 * @param   string  $payment_type
	 * @param   string  $reference_id
	 * @param   array  $order
	 * @param   string  $order_status_id
	 * @return  string
	 */
	function doUpdateStatus($status, &$payment_type, $reference_id, $order, &$order_status_id) {
		$result = GenericshopPaymentCore::updateStatus($reference_id, $order);
		if(!$result["is_valid"]) {
			$transaction_result = $result;
			$transaction_result['result'] = '';
		} else {
			$xml_result = simplexml_load_string($result["response"]);
			$code_result = (string)$xml_result->Result->Transaction->Processing->Return['code'];
			$transaction_result['result'] = GenericshopPaymentCore::getTransactionResult($code_result);
			if ($transaction_result['result'] == 'ACK') {
				if (GenericshopPaymentCore::isSuccessReview($code_result)) {
					$order_status_id = $status['review_id'];
				} else {
					$payment_code = (string)$xml_result->Result->Transaction->Payment['code'];
					$payment_type = substr($payment_code, -2);
					if ($payment_type == 'PA') {
						$order_status_id = $status['pa_id'];
					}
					if ($payment_type == 'DB') {
						$order_status_id = $status['accept_id'];
					}
				}
			}
		}
		return $transaction_result;
	}

	/**
	 * Do back office operation
	 *
	 * @param   array  $genericshop_order
	 * @param   string  $payment_type
	 * @param   string  $reference_id
	 * @param   array  $order
	 * @return  string
	 */
	function doBackOfficeOperation($genericshop_order, $payment_type, $reference_id, $order) {
		$order['currency'] = $genericshop_order['currency'];
		$order['amount'] = $genericshop_order['amount'];
		$order['payment_type'] = $payment_type;

		$response = GenericshopPaymentCore::backOfficeOperation($reference_id, $order);
		if(!$response['is_valid']){
			$transaction_result = $response;
			$transaction_result["result"] = "";
		}else{
			$code_result = $response["response"]["result"]["code"];
			$transaction_result["result"] = GenericshopPaymentCore::getTransactionResult($code_result);
			$transaction_result["is_valid"] = true;
		}
		return $transaction_result;
	}

	/**
	 * Do capture and refund Payment
	 *
	 * @param   string  $order_id
	 * @param   string  $order_status_id
	 * @return  array
	 */
	public function doBackendPayment($order_id, $order_status_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "payment_genericshop_orders WHERE orders_id = '" . (int)$order_id . "'");
		$genericshop_order = $query->row;

		if (empty($genericshop_order)) {
			return array('status' => false, 'errorMessage' => 'ERROR_' . $this->getMessageIdentifier());
		}

		$status['accept_id'] = $this->config->get('payment_genericshop_ageneral_accept_status_id');
		$status['pa_id'] = $this->config->get('payment_genericshop_ageneral_pa_status_id');
		$status['review_id'] = $this->config->get('payment_genericshop_ageneral_review_status_id');
		$status['refund_id'] = $this->config->get('payment_genericshop_ageneral_refund_status_id');

		$payment_method = $genericshop_order['payment_method'];
		$payment_type = '';

		if ($genericshop_order['payment_type'] == 'IR' && $order_status_id == $status['review_id']) {
			$payment_type = 'IR';
		}
		if ($genericshop_order['payment_type'] == 'PA' && $order_status_id == $status['accept_id']) {
			$payment_type = 'CP';
		}
		if (($genericshop_order['payment_type'] == 'DB' || $genericshop_order['payment_type'] == 'CP') && $order_status_id == $status['refund_id']) {
			$payment_type = 'RF';
		}

		$payment_result['order_status_id'] = $order_status_id;

		if (empty($payment_type)) {
			$payment_result['successMessage'] = 'text_success';
			$payment_result['status'] = true;
			return $payment_result;
		} else {
			$reference_id = $genericshop_order['unique_id'];
			$order = $this->getCredentials($payment_method);
			if ($payment_type == 'IR') {
				$transaction_result = $this->doUpdateStatus($status, $payment_type, $reference_id, $order, $order_status_id);
				$payment_result['order_status_id'] = $order_status_id;
			} else {
				if ($this->isMultiChannel($payment_method)) {
					$order['channel_id'] = $order['channel_id_moto'];
				}
				$transaction_result = $this->doBackOfficeOperation($genericshop_order, $payment_type, $reference_id, $order);
			}

			if ($transaction_result["result"] == 'ACK') {
				$this->db->query("update " . DB_PREFIX . "payment_genericshop_orders set payment_type ='" . $this->db->escape($payment_type) . "' where orders_id = " . (int)$order_id);
				$payment_result['successMessage'] = 'SUCCESS_' . $this->getMessageIdentifier($payment_type);
				$payment_result['status'] = true;
				return $payment_result;
			} else {
				if(!$transaction_result["is_valid"]){
					$payment_result['errorMessage'] = $transaction_result["response"];
					$payment_result['status'] = $transaction_result["is_valid"];
				}else{
					$payment_result['errorMessage'] = 'ERROR_' . $this->getMessageIdentifier($payment_type);
					$payment_result['status'] = false;
				}
				return $payment_result;
			}
		}
	}

	/**
	 * get a message identifier
	 *
	 * @param   string  $payment_type
	 * @return  string
	 */
	function getMessageIdentifier($payment_type = '') {
		switch ($payment_type) {
			case 'CP':
				$message_identifier = 'GENERAL_CAPTURE_PAYMENT';
				break;
			case 'RF':
				$message_identifier = 'GENERAL_REFUND_PAYMENT';
				break;
			default:
				$message_identifier = 'GENERAL_UPDATE_PAYMENT';
				break;
		}
		return $message_identifier;
	}

	/**
	 * Get the order data to create backend order
	 *
	 * @param   string  $order_info
	 * @param   string  $payment_method
	 * @param   string  $payment_type
	 * @return  array
	 */
	function getCreateOrderData($order_info, $payment_method, $payment_type) {
		$order = array();
		$order['server_mode'] = $this->config->get('payment_' . $payment_method . '_server');
		$order['login'] = $this->config->get('payment_genericshop_ageneral_login');
		$order['password'] = $this->config->get('payment_genericshop_ageneral_password');
		$order['channel_id'] = $this->config->get('payment_' . $payment_method . '_channel_id');

		$order['test_mode'] = $this->getTestMode($payment_method);

		$order['amount'] = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false);
		$order['currency'] = $order_info['currency_code'];
		$order['payment_type'] = $payment_type;
		$order['transaction_id'] = $order_info['order_id'];
		$order['payment_recurring'] = 'REPEATED';

		return $order;
	}

	/**
	 * create backend order
	 *
	 * @param   string  $order_id
	 * @param   string  $recurring_id
	 * @return  array
	 */
	public function createBackendOrder($order_id, $recurring_id=0) {
		$this->load->language('extension/payment/genericshop');
		$this->load->model('checkout/order');
		$order_info = $this->model_checkout_order->getOrder($order_id);
		$payment_method = $order_info['payment_code'];
		$registration_id = $this->getReferenceId($recurring_id);

		$accept_id = $this->config->get('payment_genericshop_ageneral_accept_status_id');
		$pa_id = $this->config->get('payment_genericshop_ageneral_pa_status_id');

		$payment_type = $this->config->get('payment_' . $payment_method . '_transaction_mode');

		$order = $this->getCreateOrderData($order_info, $payment_method, $payment_type);

		$payment_status = GenericshopPaymentCore::useRegisteredAccount($registration_id, $order);
		if(!$payment_status['is_valid']) {
			$order_result['errorMessage'] = $payment_status['response'];
			$order_result['status'] = $payment_status['is_valid'];
			return $order_result;
		}else{
			$code_result = $payment_status["response"]["result"]["code"];
			$transaction_result = GenericshopPaymentCore::getTransactionResult($code_result);

			$order_result['order_status_id'] = $this->getOrderStatusFailed();

			$order_result['comment'] = $this->language->get('BACKEND_TT_TRANSACTION_ID') . ': ' . $payment_status['response']['merchantTransactionId'] . '<br \>';

			if ($transaction_result == "ACK") {
				if ($payment_type == 'DB') {
					$order_result['order_status_id'] = $accept_id;
				}
				if ($payment_type == 'PA') {
					$order_result['order_status_id'] = $pa_id;
				}

				$this->addGenericOrder($order_id, $payment_method, $payment_status["response"]);
				$order_result['status'] = true;
				return $order_result;
			}
			$order_result['status'] = false;
			return $order_result;
		}
	}

	/**
	 * add the Generic Shop order data into the database
	 *
	 * @param   string  $order_id
	 * @param   string  $payment_method
	 * @param   array  $payment_status
	 * @return  array
	 */
	public function addGenericOrder($order_id, $payment_method, $payment_status) {
		$data['orders_id'] = $order_id;
		$data['payment_method'] = $payment_method;
		$data['unique_id'] =  $payment_status['id'];
		$data['amount'] = $payment_status['amount'];
		$data['currency'] = $payment_status['currency'];
		$data['payment_type'] = $payment_status['paymentType'];

		$this->saveOrder($data);
	}

	/**
	 * Get a test mode
	 *
	 * @param   string  $payment_method
	 * @return  boolean
	 */
	public function getTestMode($payment_method) {
		if ($this->config->get('payment_' . $payment_method . '_server') != "LIVE") {
			if ($payment_method) {
				return 'EXTERNAL';
			}
		}
		return false;
	}

	/**
	 * Check the multichannel activation of payment methods
	 *
	 * @param   string  $payment_method
	 * @return  boolean
	 */
	function isMultiChannel($payment_method) {
		return $this->config->get('payment_' . $payment_method . '_multichannel');
	}

	/**
	 * Get the credentials of payment methods
	 *
	 * @param   string  $payment_method
	 * @return  array
	 */
	function getCredentials($payment_method) {
		$credentials = array();
		$credentials['server_mode'] = $this->config->get('payment_' . $payment_method . '_server');
		$credentials['login'] = $this->config->get('payment_genericshop_ageneral_login');
		$credentials['password'] = $this->config->get('payment_genericshop_ageneral_password');
		$credentials['test_mode'] = $this->getTestMode('payment_' . $payment_method);
		$credentials['channel_id'] = $this->config->get('payment_' . $payment_method . '_channel_id');
		if ($this->isMultiChannel($payment_method)) {
			$credentials['channel_id_moto'] = $this->config->get('payment_' . $payment_method . '_moto');
		}

		return $credentials;
	}

	/**
	 * Get the product price
	 *
	 * @param   string  $product_id
	 * @return  boolean|array
	 */
	function getProductPrice($product_id)
	{
		$query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product_id . "'");
		if ($query->num_rows) {
			return $query->row['price'];
		}
		return false;
	}

	public function getCartAmount(){
		$this->load->model('setting/extension');

		$totals = array();
		$taxes = $this->cart->getTaxes();
		$total = 0;

		// Because __call can not keep var references so we put them into an array.
		$total_data = array(
			'totals' => &$totals,
			'taxes'  => &$taxes,
			'total'  => &$total
		);

		// Display prices
		if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
			$sort_order = array();

			$results = $this->model_setting_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get('total_' . $result['code'] . '_status')) {
					$this->load->model('extension/total/' . $result['code']);

					// We have to put the totals in an array so that they pass by reference.
					$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
				}
			}

			$sort_order = array();

			foreach ($totals as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $totals);
		}

		return $totals;
	}
}
