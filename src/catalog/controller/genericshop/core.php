<?php
/**
 * Generic Shop OPP Core
 *
 * This Class for Process Opp Payment Gateways
 * Copyright ( c ) Generic Shop
 *
 * @class      GenericshopPaymentCore
 * @package    Genericshop/Classes
 * @located at /includes/core/
 */
class GenericshopPaymentCore
{
	protected static $checkout_url_live = 'https://oppwa.com/v1/checkouts';
	protected static $checkout_url_test = 'https://test.oppwa.com/v1/checkouts';

	protected static $back_office_url_live = 'https://oppwa.com/v1/payments/';
	protected static $back_office_url_test = 'https://test.oppwa.com/v1/payments/';

	protected static $payment_widget_url_live = 'https://oppwa.com/v1/paymentWidgets.js?checkoutId=';
	protected static $payment_widget_url_test = 'https://test.oppwa.com/v1/paymentWidgets.js?checkoutId=';

	protected static $register_url_live = 'https://oppwa.com/v1/registrations/';
	protected static $register_url_test = 'https://test.oppwa.com/v1/registrations/';

	protected static $query_url_live = 'https://ctpe.net/payment/query';
	protected static $query_url_test = 'https://test.ctpe.net/payment/query';

	protected static $ack_return_codes = array (
		'000.000.000',
		'000.100.110',
		'000.100.111',
		'000.100.112',
		'000.100.200',
		'000.100.201',
		'000.100.202',
		'000.100.203',
		'000.100.204',
		'000.100.205',
		'000.100.206',
		'000.100.207',
		'000.100.208',
		'000.100.209',
		'000.100.210',
		'000.100.220',
		'000.100.221',
		'000.100.222',
		'000.100.223',
		'000.100.224',
		'000.100.225',
		'000.100.226',
		'000.100.227',
		'000.100.228',
		'000.100.229',
		'000.100.230',
		'000.100.299',
		'000.200.000',
		'000.300.000',
		'000.300.100',
		'000.300.101',
		'000.300.102',
		'000.400.000',
		'000.400.010',
		'000.400.020',
		'000.400.030',
		'000.400.040',
		'000.400.050',
		'000.400.060',
		'000.400.070',
		'000.400.080',
		'000.400.090',
		'000.400.101',
		'000.400.102',
		'000.400.103',
		'000.400.104',
		'000.400.105',
		'000.400.106',
		'000.400.107',
		'000.400.108',
		'000.400.200',
		'000.500.000',
		'000.500.100',
		'000.600.000'
	);

	protected static $nok_return_codes = array (
		'100.100.100',
		'100.100.101',
		'100.100.200',
		'100.100.201',
		'100.100.300',
		'100.100.301',
		'100.100.303',
		'100.100.304',
		'100.100.400',
		'100.100.401',
		'100.100.402',
		'100.100.500',
		'100.100.501',
		'100.100.600',
		'100.100.601',
		'100.100.650',
		'100.100.651',
		'100.100.700',
		'100.100.701',
		'100.150.100',
		'100.150.101',
		'100.150.200',
		'100.150.201',
		'100.150.202',
		'100.150.203',
		'100.150.204',
		'100.150.205',
		'100.150.300',
		'100.200.100',
		'100.200.103',
		'100.200.104',
		'100.200.200',
		'100.210.101',
		'100.210.102',
		'100.211.101',
		'100.211.102',
		'100.211.103',
		'100.211.104',
		'100.211.105',
		'100.211.106',
		'100.212.101',
		'100.212.102',
		'100.212.103',
		'100.250.100',
		'100.250.105',
		'100.250.106',
		'100.250.107',
		'100.250.110',
		'100.250.111',
		'100.250.120',
		'100.250.121',
		'100.250.122',
		'100.250.123',
		'100.250.124',
		'100.250.125',
		'100.250.250',
		'100.300.101',
		'100.300.200',
		'100.300.300',
		'100.300.400',
		'100.300.401',
		'100.300.402',
		'100.300.501',
		'100.300.600',
		'100.300.601',
		'100.300.700',
		'100.300.701',
		'100.350.100',
		'100.350.101',
		'100.350.200',
		'100.350.201',
		'100.350.301',
		'100.350.302',
		'100.350.303',
		'100.350.310',
		'100.350.311',
		'100.350.312',
		'100.350.313',
		'100.350.314',
		'100.350.315',
		'100.350.400',
		'100.350.500',
		'100.350.600',
		'100.350.601',
		'100.350.610',
		'100.360.201',
		'100.360.300',
		'100.360.303',
		'100.360.400',
		'100.370.100',
		'100.370.101',
		'100.370.102',
		'100.370.110',
		'100.370.111',
		'100.370.121',
		'100.370.122',
		'100.370.123',
		'100.370.124',
		'100.370.125',
		'100.370.131',
		'100.370.132',
		'100.380.100',
		'100.380.101',
		'100.380.110',
		'100.380.201',
		'100.380.305',
		'100.380.306',
		'100.380.401',
		'100.380.501',
		'100.390.101',
		'100.390.102',
		'100.390.103',
		'100.390.104',
		'100.390.105',
		'100.390.106',
		'100.390.107',
		'100.390.108',
		'100.390.109',
		'100.390.110',
		'100.390.111',
		'100.390.112',
		'100.390.113',
		'100.395.101',
		'100.395.102',
		'100.395.501',
		'100.395.502',
		'100.396.101',
		'100.396.102',
		'100.396.103',
		'100.396.104',
		'100.396.106',
		'100.396.201',
		'100.397.101',
		'100.397.102',
		'100.400.000',
		'100.400.001',
		'100.400.002',
		'100.400.005',
		'100.400.007',
		'100.400.020',
		'100.400.021',
		'100.400.030',
		'100.400.039',
		'100.400.040',
		'100.400.041',
		'100.400.042',
		'100.400.043',
		'100.400.044',
		'100.400.045',
		'100.400.051',
		'100.400.060',
		'100.400.061',
		'100.400.063',
		'100.400.064',
		'100.400.065',
		'100.400.071',
		'100.400.080',
		'100.400.081',
		'100.400.083',
		'100.400.084',
		'100.400.085',
		'100.400.086',
		'100.400.087',
		'100.400.091',
		'100.400.100',
		'100.400.120',
		'100.400.121',
		'100.400.122',
		'100.400.123',
		'100.400.130',
		'100.400.139',
		'100.400.140',
		'100.400.141',
		'100.400.142',
		'100.400.143',
		'100.400.144',
		'100.400.145',
		'100.400.146',
		'100.400.147',
		'100.400.148',
		'100.400.149',
		'100.400.150',
		'100.400.151',
		'100.400.152',
		'100.400.241',
		'100.400.242',
		'100.400.243',
		'100.400.260',
		'100.400.300',
		'100.400.301',
		'100.400.302',
		'100.400.303',
		'100.400.304',
		'100.400.305',
		'100.400.306',
		'100.400.307',
		'100.400.308',
		'100.400.309',
		'100.400.310',
		'100.400.311',
		'100.400.312',
		'100.400.313',
		'100.400.314',
		'100.400.315',
		'100.400.316',
		'100.400.317',
		'100.400.318',
		'100.400.319',
		'100.400.320',
		'100.400.321',
		'100.400.322',
		'100.400.323',
		'100.400.324',
		'100.400.325',
		'100.400.326',
		'100.400.327',
		'100.400.328',
		'100.400.500',
		'100.500.101',
		'100.500.201',
		'100.500.301',
		'100.500.302',
		'100.550.300',
		'100.550.301',
		'100.550.303',
		'100.550.310',
		'100.550.311',
		'100.550.312',
		'100.550.400',
		'100.550.401',
		'100.550.601',
		'100.550.603',
		'100.550.605',
		'100.600.500',
		'100.700.100',
		'100.700.101',
		'100.700.200',
		'100.700.201',
		'100.700.300',
		'100.700.400',
		'100.700.500',
		'100.700.800',
		'100.700.801',
		'100.700.802',
		'100.700.810',
		'100.800.100',
		'100.800.101',
		'100.800.102',
		'100.800.200',
		'100.800.201',
		'100.800.202',
		'100.800.300',
		'100.800.301',
		'100.800.302',
		'100.800.400',
		'100.800.401',
		'100.800.500',
		'100.800.501',
		'100.900.100',
		'100.900.101',
		'100.900.105',
		'100.900.200',
		'100.900.300',
		'100.900.301',
		'100.900.400',
		'100.900.401',
		'100.900.450',
		'100.900.500',
		'200.100.101',
		'200.100.102',
		'200.100.103',
		'200.100.150',
		'200.100.151',
		'200.100.199',
		'200.100.201',
		'200.100.300',
		'200.100.301',
		'200.100.302',
		'200.100.401',
		'200.100.402',
		'200.100.403',
		'200.100.404',
		'200.100.501',
		'200.100.502',
		'200.100.503',
		'200.100.504',
		'200.200.106',
		'200.300.403',
		'200.300.404',
		'200.300.405',
		'200.300.406',
		'200.300.407',
		'500.100.201',
		'500.100.202',
		'500.100.203',
		'500.100.301',
		'500.100.302',
		'500.100.303',
		'500.100.304',
		'500.100.401',
		'500.100.402',
		'500.100.403',
		'500.200.101',
		'600.100.100',
		'600.200.100',
		'600.200.200',
		'600.200.201',
		'600.200.202',
		'600.200.300',
		'600.200.310',
		'600.200.400',
		'600.200.500',
		'600.200.600',
		'600.200.700',
		'600.200.800',
		'600.200.810',
		'700.100.100',
		'700.100.200',
		'700.100.300',
		'700.100.400',
		'700.100.500',
		'700.100.600',
		'700.100.700',
		'700.100.701',
		'700.100.710',
		'700.300.100',
		'700.300.200',
		'700.300.300',
		'700.300.400',
		'700.300.500',
		'700.300.600',
		'700.300.700',
		'700.400.000',
		'700.400.100',
		'700.400.101',
		'700.400.200',
		'700.400.300',
		'700.400.400',
		'700.400.402',
		'700.400.410',
		'700.400.420',
		'700.400.510',
		'700.400.520',
		'700.400.530',
		'700.400.540',
		'700.400.550',
		'700.400.560',
		'700.400.561',
		'700.400.562',
		'700.400.570',
		'700.400.700',
		'700.450.001',
		'800.100.100',
		'800.100.150',
		'800.100.151',
		'800.100.152',
		'800.100.153',
		'800.100.154',
		'800.100.155',
		'800.100.156',
		'800.100.157',
		'800.100.158',
		'800.100.159',
		'800.100.160',
		'800.100.161',
		'800.100.162',
		'800.100.163',
		'800.100.164',
		'800.100.165',
		'800.100.166',
		'800.100.167',
		'800.100.168',
		'800.100.169',
		'800.100.170',
		'800.100.171',
		'800.100.172',
		'800.100.173',
		'800.100.174',
		'800.100.175',
		'800.100.176',
		'800.100.177',
		'800.100.178',
		'800.100.179',
		'800.100.190',
		'800.100.191',
		'800.100.192',
		'800.100.195',
		'800.100.196',
		'800.100.197',
		'800.100.198',
		'800.100.402',
		'800.100.500',
		'800.100.501',
		'800.110.100',
		'800.120.100',
		'800.120.101',
		'800.120.102',
		'800.120.103',
		'800.120.200',
		'800.120.201',
		'800.120.202',
		'800.120.203',
		'800.120.300',
		'800.120.401',
		'800.121.100',
		'800.130.100',
		'800.140.100',
		'800.140.101',
		'800.140.110',
		'800.140.111',
		'800.140.112',
		'800.140.113',
		'800.150.100',
		'800.160.100',
		'800.160.110',
		'800.160.120',
		'800.160.130',
		'800.200.159',
		'800.200.160',
		'800.200.165',
		'800.200.202',
		'800.200.208',
		'800.200.220',
		'800.300.101',
		'800.300.102',
		'800.300.200',
		'800.300.301',
		'800.300.302',
		'800.300.401',
		'800.300.500',
		'800.300.501',
		'800.400.100',
		'800.400.101',
		'800.400.102',
		'800.400.103',
		'800.400.104',
		'800.400.105',
		'800.400.110',
		'800.400.150',
		'800.400.151',
		'800.400.200',
		'800.400.500',
		'800.500.100',
		'800.500.110',
		'800.600.100',
		'800.700.100',
		'800.700.101',
		'800.700.201',
		'800.700.500',
		'800.800.102',
		'800.800.202',
		'800.800.302',
		'800.800.800',
		'800.800.801',
		'800.900.100',
		'800.900.101',
		'800.900.200',
		'800.900.201',
		'800.900.300',
		'800.900.301',
		'800.900.302',
		'800.900.303',
		'800.900.401',
		'800.900.450',
		'900.100.100',
		'900.100.200',
		'900.100.201',
		'900.100.202',
		'900.100.203',
		'900.100.300',
		'900.100.400',
		'900.100.500',
		'900.100.600',
		'900.200.100',
		'900.300.600',
		'900.400.100',
		'999.999.999'
	);

	/**
	 * Get the URL to checkout payment based on the server mode
	 *
	 * @param   string  $server_mode
	 * @return  string
	 */
	private static function getCheckoutUrl($server_mode)
	{
		if ($server_mode == "LIVE") {
			return self::$checkout_url_live;
		} else {
			return self::$checkout_url_test;
		}
	}

	/**
	 * Get the payment status URL to receive responses from the payment gateway
	 *
	 * @param   string  $server_mode
	 * @param   string  $checkout_id
	 * @return  string
	 */
	private static function getPaymentStatusUrl($server_mode, $checkout_id)
	{
		if ($server_mode == "LIVE") {
			return self::$checkout_url_live . '/' . $checkout_id . "/payment";
		} else {
			return self::$checkout_url_test . '/' . $checkout_id . "/payment";
		}
	}

	/**
	 * Get the back office URL based on the server mode
	 *
	 * @param   string  $server_mode
	 * @param   string  $reference_id
	 * @return  string
	 */
	private static function getBackOfficeUrl($server_mode, $reference_id)
	{
		if ($server_mode == "LIVE") {
			return self::$back_office_url_live . $reference_id;
		} else {
			return self::$back_office_url_test . $reference_id;
		}
	}

	/**
	 * Get the URL to use a registered account
	 *
	 * @param   string  $server_mode
	 * @param   string  $reference_id
	 * @return  string
	 */
	private static function getUrlToUseRegisteredAccount($server_mode, $reference_id)
	{
		if ($server_mode=="LIVE") {
			return self::$register_url_live . $reference_id . '/payments';
		} else {
			return self::$register_url_test . $reference_id . '/payments';
		}
	}

	/**
	 * Get the deregister URL based on the server mode
	 *
	 * @param   string  $server_mode
	 * @param   string  $reference_id
	 * @return  string
	 */
	private static function getDeregisterUrl($server_mode, $reference_id)
	{
		if ($server_mode=="LIVE") {
			return self::$register_url_live . $reference_id;
		} else {
			return self::$register_url_test . $reference_id;
		}
	}

	/**
	 * Get the query URL based on the server mode
	 *
	 * @param   string  $server_mode
	 * @param   string  $reference_id
	 * @return  string
	 */
	private static function getQueryUrl($server_mode)
	{
		if ($server_mode=="LIVE") {
			return self::$query_url_live;
		} else {
			return self::$query_url_test;
		}
	}

	/**
	 * execute curl and return the response from gateway
	 * @param  boolean $is_json_decoded
	 * @return array
	 */
	private static function getCurlResponse($ch, $is_json_decoded = true)
	{
		$response_body = curl_exec($ch);
		$curl_response = array();
		$curl_error_no = curl_errno($ch);

		if ($curl_error_no) {
			$curl_response['response'] = self::_getCurlErrorIdentifier($curl_error_no);
			$curl_response['is_valid'] = false;
		} elseif (!empty($response_body)) {
			if ($is_json_decoded) {
				$curl_response['response'] = json_decode($response_body, true);
				$curl_response['is_valid'] = true;
			} else {
				$curl_response['response'] = $response_body;
				$curl_response['is_valid'] = true;
			}
		} else {
			$curl_response['response'] = 'ERROR_GENERAL_NORESPONSE';
			$curl_response['is_valid'] = false;
		}

		curl_close($ch);

		return $curl_response;
	}


	/**
	 * Get a response data from the gateway
	 *
	 * @param string $data
	 * @param string $url
	 * @param string $server_mode
	 * @return array
	 */
	private static function getResponseData($data, $url, $server_mode)
	{
		$ch = curl_init();
		self::_setSslVerifypeer($ch, $server_mode);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		return self::getCurlResponse($ch, $is_json_decoded = true);
	}

	/**
	 * Get a payment response from the gateway
	 *
	 * @param string $url
	 * @param string $server_mode
	 * @return array
	 */
	private static function getPaymentResponse($url, $server_mode)
	{
		$ch = curl_init();
		self::_setSslVerifypeer($ch, $server_mode);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

		return self::getCurlResponse($ch, $is_json_decoded = true);
	}

	/**
	 * Send the deregistration payment account to the gateway
	 *
	 * @param string $url
	 * @param string $server_mode
	 * @return array
	 */
	private static function sendDeregistration($url, $server_mode)
	{
		$ch = curl_init();
		self::_setSslVerifypeer($ch, $server_mode);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		return self::getCurlResponse($ch, $is_json_decoded = true);
	}

	/**
	 * Get a current payment status to the gateway
	 *
	 * @param string $url
	 * @param string $xml_request
	 * @param string $server_mode
	 * @return array
	 */
	private static function getCurrentPaymentStatus($url, $xml_request, $server_mode)
	{
		$ch = curl_init();
		self::_setSslVerifypeer($ch, $server_mode);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/x-www-form-urlencoded;charset=UTF-8'));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_request);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		return self::getCurlResponse($ch, $is_json_decoded = false);
	}

	 /**
	 * return content from payment widget url
	 *
	 * @param string $payment_widget_url
	 * @param string $server_mode
	 * @return string
	 */
	public static function getPaymentWidgetContent($payment_widget_url, $server_mode)
	{
		$ch = curl_init();
		self::_setSslVerifypeer($ch, $server_mode);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $payment_widget_url);

		return self::getCurlResponse($ch, false);
	}

	/**
	 * Sets the ssl verifypeer.
	 *
	 * @param      curl  $ch
	 * @param      string  $server_mode  The server mode
	 */
	private static function _setSslVerifypeer(&$ch, $server_mode = 'LIVE')
	{
		if ($server_mode == 'TEST') {
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		}
	}

	/**
	 * get Curl error identifier translation
	 *
	 * @param string $code
	 * @return string
	 */
	private static function _getCurlErrorIdentifier($code)
	{
		$error_messages = array(
			'60' => 'ERROR_MERCHANT_SSL_CERTIFICATE'
		);

		if ($code) {
			return array_key_exists($code, $error_messages) ? $error_messages[$code] : 'ERROR_GENERAL_NORESPONSE';
		} else {
			return 'ERROR_GENERAL_GENERAL';
		}
	}

	/**
	 * Get the payment credential to send request to the gateway
	 *
	 * @param array $order
	 * @return array
	 */
	private static function getPaymentCredential($order)
	{
		$parameters = array();
		$parameters['authentication.userId'] = $order['login'];
		$parameters['authentication.password'] = $order['password'];
		$parameters['authentication.entityId'] = $order['channel_id'];

		// test mode parameters (true)
		if (!empty($order['test_mode'])) {
			$parameters['testMode'] = $order['test_mode'];
		}

		return $parameters;
	}

	/**
	 * Set the separator for the decimal point
	 * Set the number of decimal points
	 *
	 * @param string|float $number
	 * @return string
	 */
	public static function setNumberFormat($number)
	{
		return number_format(str_replace(',', '.', $number), 2, '.', '');
	}

	/**
	 * Set cart items parameters
	 *
	 * @param array $cart_items
	 * @return array
	 */
	private static function setCartItemsParameters($cart_items)
	{
		$parameters = array();
		for ($i=0; $i < count($cart_items); $i++) {
			$parameters['cart.items[' . $i . '].merchantItemId'] = $cart_items[$i]['merchant_item_id'];
			if (isset($cart_items[$i]['discount'])) {
				$parameters['cart.items[' . $i . '].discount'] = self::setNumberFormat($cart_items[$i]['discount']);
			}
			$parameters['cart.items[' . $i . '].quantity'] = $cart_items[$i]['quantity'];
			$parameters['cart.items[' . $i . '].name'] = $cart_items[$i]['name'];
			$parameters['cart.items[' . $i . '].price'] = self::setNumberFormat($cart_items[$i]['price']);
			if (isset($cart_items[$i]['tax'])) {
				$parameters['cart.items[' . $i . '].tax'] = self::setNumberFormat($cart_items[$i]['tax']);
			}
		}
		return $parameters;
	}

	/**
	 * Set checkout parameters
	 *
	 * @param array $order
	 * @return string
	 */
	private static function setCheckoutParameters($transaction_data)
	{
		$parameters = array();
		$parameters = self::getPaymentCredential($transaction_data);
		$parameters['merchantTransactionId'] = $transaction_data['transaction_id'];
		$parameters['customer.email'] = $transaction_data['customer']['email'];
		$parameters['customer.givenName'] = $transaction_data['customer']['first_name'];
		$parameters['customer.surname'] = $transaction_data['customer']['last_name'];
		$parameters['billing.street1'] = $transaction_data['billing']['street'];
		$parameters['billing.city'] = $transaction_data['billing']['city'];
		$parameters['billing.postcode'] = $transaction_data['billing']['zip'];
		$parameters['billing.country'] = $transaction_data['billing']['country_code'];
		$parameters['amount'] = self::setNumberFormat($transaction_data['amount']);
		$parameters['currency'] = $transaction_data['currency'];

		if (!empty($transaction_data['customer']['sex'])) {
			$parameters['customer.sex'] = $transaction_data['customer']['sex'];
		}
		if (!empty($transaction_data['customer']['birthdate'])) {
			$parameters['customer.birthDate'] = $transaction_data['customer']['birthdate'];
		}
		if (!empty($transaction_data['customer']['phone'])) {
			$parameters['customer.phone'] = $transaction_data['customer']['phone'];
		}
		if (!empty($transaction_data['customer']['mobile'])) {
			$parameters['customer.mobile'] = $transaction_data['customer']['mobile'];
		}

		if (!empty($transaction_data['cartItems'])) {
			$parameters = array_merge($parameters, self::setCartItemsParameters($transaction_data['cartItems']));
		}

		// payment type for RG.DB or only RG
		if (!empty($transaction_data['payment_type'])) {
			  $parameters['paymentType'] = $transaction_data['payment_type'];
		}

		// registration parameter (true)
		if (!empty($transaction_data['payment_registration'])) {
			$parameters['createRegistration'] = $transaction_data['payment_registration'];
			if (!empty($transaction_data['3D'])) {
				  $parameters['customParameters[presentation.amount3D]'] =
				  self::setNumberFormat($transaction_data['3D']['amount']);
				  $parameters['customParameters[presentation.currency3D]'] =
				  $transaction_data['3D']['currency'];
			}
		}

		// recurring payment parameters : initial/repeated
		if (!empty($transaction_data['payment_recurring'])) {
			$parameters['recurringType'] = $transaction_data['payment_recurring'];
		}

		if (!empty($transaction_data['customer_ip'])) {
			$parameters['customer.ip'] = $transaction_data['customer_ip'];
		}

		if (!empty($transaction_data['registrations'])) {
			foreach ($transaction_data['registrations'] as $key => $value) {
				  $parameters['registrations[' . $key . '].id'] = $value;
			}
		}

		return http_build_query($parameters);
	}

	/**
	 * Set parameters to use registered payment account
	 *
	 * @param array $order
	 * @return array
	 */
	private static function setRegisteredAccountParameters($order)
	{
		$parameters = array();
		$parameters = self::getPaymentCredential($order);
		$parameters['amount'] = self::setNumberFormat($order['amount']);
		$parameters['currency'] = $order['currency'];
		$parameters['paymentType'] = $order['payment_type'];
		$parameters['merchantTransactionId'] = $order['transaction_id'];
		$parameters['recurringType'] = $order['payment_recurring'];

		return http_build_query($parameters);
	}

	/**
	 * Set back office parameters
	 *
	 * @param array $order
	 * @return array
	 */
	private static function setBackOfficeParameter($order)
	{
		$parameters = array();
		$parameters = self::getPaymentCredential($order);
		$parameters['paymentType'] = $order['payment_type'];

		//Reversal (RV) didn't send amount & currency parameter
		if ($order['payment_type'] != 'RV') {
			$parameters['amount'] = self::setNumberFormat($order['amount']);
			$parameters['currency'] = $order['currency'];
		}

		return http_build_query($parameters);
	}

	/**
	 * Get a XML current payment status
	 *
	 * @param string $reference_id
	 * @param array $order
	 * @return xml
	 */
	private static function getXmlCurrentPaymentStatus($reference_id, $order)
	{
		switch ($order['test_mode']) {
			case 'INTERNAL':
				$mode = 'INTEGRATOR_TEST';
				break;
			case 'EXTERNAL':
				$mode = 'CONNECTOR_TEST';
				break;
			default:
				$mode = 'LIVE';
				break;
		}

		$xml = 'load=<?xml version="1.0" encoding="UTF-8"?><Request version="1.0">
			<Header><Security sender="' . $order['channel_id'] . '"/></Header>
			<Query mode="' . $mode . '" level="CHANNEL" entity="' . $order['channel_id'] . '" type="STANDARD">
				<User login="' . $order['login'] . '" pwd="' . $order['password'] . '"/>
				<Identification>
					<UniqueID>' . $reference_id . '</UniqueID>
				</Identification>
				</Query>
			</Request>';

		return $xml;
	}

	/**
	 * Check if the plugin gets a response from the gateway in 3 trials
	 *
	 * @param string $payment_status_url
	 * @param array $payment_response
	 * @param array $server_mode
	 * @return array $payment_response
	 * @return boolean
	 */
	private static function isPaymentGetResponse($payment_status_url, &$payment_response, $server_mode)
	{
		for ($i=0; $i < 3; $i++) {
			$response = true;
			try {
				$payment_response = self::getPaymentResponse($payment_status_url, $server_mode);
			} catch (Exception $e) {
				$response = false;
			}
			if ($response && $payment_response["response"]) {
				return true;
				break;
			}
		}
		return false;
	}

	/**
	 * Prepare the checkout & get payment widget URL (for create payment form)
	 *
	 * @param array $order
	 * @return string $checkout_result
	 * @return string
	 */
	public static function getPaymentWidgetUrl($order, $checkout_id)
	{
		if ($order['server_mode'] == 'LIVE') {
			return self::$payment_widget_url_live . $checkout_id;
		} else {
			return self::$payment_widget_url_test . $checkout_id;
		}
	}

	/**
		* this function use to send checkout request to gateway
		*
		* @param array $order
		* @return array
	*/
	public static function getCheckoutResult($order)
	{
		//prepareCheckout
		$checkout_url = self::getCheckoutUrl($order['server_mode']);
		$post_data = self::setCheckoutParameters($order);

		return self::getResponseData($post_data, $checkout_url, $order['server_mode']);
	}

	/**
	 * Get a Payment Status
	 *
	 * @param string $checkout_id
	 * @param array $order
	 * @param string $server_mode
	 * @return string|boolean
	 */
	public static function getPaymentStatus($checkout_id, $order, $server_mode)
	{
		$payment_status_url = self::getPaymentStatusUrl($order['server_mode'], $checkout_id);
		$payment_status_url .= '?' . http_build_query(self::getPaymentCredential($order));
		$response = self::isPaymentGetResponse($payment_status_url, $payment_response, $order["server_mode"]);

		if ($response) {
			return $payment_response;
		}

		return false;
	}

	/**
	 * Use the registered payment account
	 *
	 * @param string $reference_id
	 * @param array $order
	 * @return string|boolean
	 */
	public static function useRegisteredAccount($reference_id, $order)
	{
		$register_url = self::getUrlToUseRegisteredAccount($order['server_mode'], $reference_id);
		$post_data = self::setRegisteredAccountParameters($order);
		$response_data = self::getResponseData($post_data, $register_url, $order['server_mode']);

		return $response_data;
	}

	/**
	 * Back office operations such ad capture and refund
	 *
	 * @param string $reference_id
	 * @param array $order
	 * @return string|boolean
	 */
	public static function backOfficeOperation($reference_id, $order)
	{
		$back_office_url = self::getBackOfficeUrl($order['server_mode'], $reference_id);
		$post_data = self::setBackOfficeParameter($order);
		$response_data = self::getResponseData($post_data, $back_office_url, $order['server_mode']);

		return $response_data;
	}

	/**
	 * delete the payment account
	 *
	 * @param string $reference_id
	 * @param array $order
	 * @return string|boolean
	 */
	public static function deleteRegisteredAccount($reference_id, $order)
	{
		$deregister_url = self::getDeregisterUrl($order['server_mode'], $reference_id);
		$deregister_url .= '?' . http_build_query(self::getPaymentCredential($order));
		$deregistration_response = self::sendDeregistration($deregister_url, $order['server_mode']);

		return $deregistration_response;
	}

	/**
	 * update payment status
	 *
	 * @param string $reference_id
	 * @param array $order
	 * @return xml
	 */
	public static function updateStatus($reference_id, $order)
	{
		$query_url = self::getQueryUrl($order['server_mode']);
		$xml_current_payment_status = self::getXmlCurrentPaymentStatus($reference_id, $order);
		$current_payment_status = self::getCurrentPaymentStatus($query_url, $xml_current_payment_status, $order['server_mode']);

		return $current_payment_status;
	}

	/**
	 * Get a transaction result (ACK or NOK)
	 *
	 * @param string|boolean $return_code
	 * @return string|boolean
	 */
	public static function getTransactionResult($return_code = false)
	{
		if ($return_code) {
			if (in_array($return_code, self::$ack_return_codes)) {
				return "ACK";
			} elseif (in_array($return_code, self::$nok_return_codes)) {
				return "NOK";
			}
		}
		return false;
	}

	/**
	 * Get error identifier
	 *
	 * @param string $code
	 * @return string
	 */
	public static function getErrorIdentifier($code)
	{
		$error_messages = array(
			'800.150.100' => 'ERROR_CC_ACCOUNT',

			'800.100.402' => 'ERROR_CC_INVALIDDATA',
			'100.100.101' => 'ERROR_CC_INVALIDDATA',
			'800.100.151' => 'ERROR_CC_INVALIDDATA',
			'000.400.108' => 'ERROR_CC_INVALIDDATA',
			'100.100.100' => 'ERROR_CC_INVALIDDATA',
			'100.100.200' => 'ERROR_CC_INVALIDDATA',
			'100.100.201' => 'ERROR_CC_INVALIDDATA',
			'100.100.300' => 'ERROR_CC_INVALIDDATA',
			'100.100.301' => 'ERROR_CC_INVALIDDATA',
			'100.100.304' => 'ERROR_CC_INVALIDDATA',
			'100.100.400' => 'ERROR_CC_INVALIDDATA',
			'100.100.401' => 'ERROR_CC_INVALIDDATA',
			'100.100.402' => 'ERROR_CC_INVALIDDATA',
			'100.100.651' => 'ERROR_CC_INVALIDDATA',
			'100.100.700' => 'ERROR_CC_INVALIDDATA',
			'100.200.100' => 'ERROR_CC_INVALIDDATA',
			'100.200.103' => 'ERROR_CC_INVALIDDATA',
			'100.200.104' => 'ERROR_CC_INVALIDDATA',
			'100.400.000' => 'ERROR_CC_INVALIDDATA',
			'100.400.001' => 'ERROR_CC_INVALIDDATA',
			'100.400.086' => 'ERROR_CC_INVALIDDATA',
			'100.400.087' => 'ERROR_CC_INVALIDDATA',
			'100.400.002' => 'ERROR_CC_INVALIDDATA',
			'100.400.316' => 'ERROR_CC_INVALIDDATA',
			'100.400.317' => 'ERROR_CC_INVALIDDATA',
			'100.100.600' => 'ERROR_CC_INVALIDDATA',

			'800.300.401' => 'ERROR_CC_BLACKLIST',

			'800.100.171' => 'ERROR_CC_DECLINED_CARD',
			'800.100.165' => 'ERROR_CC_DECLINED_CARD',
			'800.100.159' => 'ERROR_CC_DECLINED_CARD',
			'800.100.195' => 'ERROR_CC_DECLINED_CARD',
			'000.400.101' => 'ERROR_CC_DECLINED_CARD',
			'100.100.501' => 'ERROR_CC_DECLINED_CARD',
			'100.100.701' => 'ERROR_CC_DECLINED_CARD',
			'100.400.005' => 'ERROR_CC_DECLINED_CARD',
			'100.400.020' => 'ERROR_CC_DECLINED_CARD',
			'100.400.021' => 'ERROR_CC_DECLINED_CARD',
			'100.400.030' => 'ERROR_CC_DECLINED_CARD',
			'100.400.039' => 'ERROR_CC_DECLINED_CARD',
			'100.400.081' => 'ERROR_CC_DECLINED_CARD',
			'100.400.100' => 'ERROR_CC_DECLINED_CARD',
			'100.400.123' => 'ERROR_CC_DECLINED_CARD',
			'100.400.319' => 'ERROR_CC_DECLINED_CARD',
			'800.100.154' => 'ERROR_CC_DECLINED_CARD',
			'800.100.156' => 'ERROR_CC_DECLINED_CARD',
			'800.100.158' => 'ERROR_CC_DECLINED_CARD',
			'800.100.160' => 'ERROR_CC_DECLINED_CARD',
			'800.100.161' => 'ERROR_CC_DECLINED_CARD',
			'800.100.163' => 'ERROR_CC_DECLINED_CARD',
			'800.100.164' => 'ERROR_CC_DECLINED_CARD',
			'800.100.166' => 'ERROR_CC_DECLINED_CARD',
			'800.100.167' => 'ERROR_CC_DECLINED_CARD',
			'800.100.169' => 'ERROR_CC_DECLINED_CARD',
			'800.100.170' => 'ERROR_CC_DECLINED_CARD',
			'800.100.173' => 'ERROR_CC_DECLINED_CARD',
			'800.100.174' => 'ERROR_CC_DECLINED_CARD',
			'800.100.175' => 'ERROR_CC_DECLINED_CARD',
			'800.100.176' => 'ERROR_CC_DECLINED_CARD',
			'800.100.177' => 'ERROR_CC_DECLINED_CARD',
			'800.100.190' => 'ERROR_CC_DECLINED_CARD',
			'800.100.191' => 'ERROR_CC_DECLINED_CARD',
			'800.100.196' => 'ERROR_CC_DECLINED_CARD',
			'800.100.197' => 'ERROR_CC_DECLINED_CARD',
			'800.100.168' => 'ERROR_CC_DECLINED_CARD',

			'100.100.303' => 'ERROR_CC_EXPIRED',

			'800.100.153' => 'ERROR_CC_INVALIDCVV',
			'100.100.601' => 'ERROR_CC_INVALIDCVV',
			'800.100.192' => 'ERROR_CC_INVALIDCVV',

			'800.100.157' => 'ERROR_CC_EXPIRY',

			'800.100.162' => 'ERROR_CC_LIMIT_EXCEED',

			'100.400.040' => 'ERROR_CC_3DAUTH',
			'100.400.060' => 'ERROR_CC_3DAUTH',
			'100.400.080' => 'ERROR_CC_3DAUTH',
			'100.400.120' => 'ERROR_CC_3DAUTH',
			'100.400.260' => 'ERROR_CC_3DAUTH',
			'800.900.300' => 'ERROR_GENERAL_REDIRECT',
			'800.900.301' => 'ERROR_CC_3DAUTH',
			'800.900.302' => 'ERROR_CC_3DAUTH',
			'100.380.401' => 'ERROR_CC_3DAUTH',

			'100.390.105' => 'ERROR_CC_3DERROR',
			'000.400.103' => 'ERROR_CC_3DERROR',
			'000.400.104' => 'ERROR_CC_3DERROR',
			'100.390.106' => 'ERROR_CC_3DERROR',
			'100.390.107' => 'ERROR_CC_3DERROR',
			'100.390.108' => 'ERROR_CC_3DERROR',
			'100.390.109' => 'ERROR_CC_3DERROR',
			'100.390.111' => 'ERROR_CC_3DERROR',
			'800.400.200' => 'ERROR_CC_3DERROR',
			'100.390.112' => 'ERROR_CC_3DERROR',

			'100.100.500' => 'ERROR_CC_NOBRAND',

			'800.100.155' => 'ERROR_GENERAL_LIMIT_AMOUNT',
			'000.100.203' => 'ERROR_GENERAL_LIMIT_AMOUNT',
			'100.550.310' => 'ERROR_GENERAL_LIMIT_AMOUNT',
			'100.550.311' => 'ERROR_GENERAL_LIMIT_AMOUNT',

			'800.120.101' => 'ERROR_GENERAL_LIMIT_TRANSACTIONS',
			'800.120.100' => 'ERROR_GENERAL_LIMIT_TRANSACTIONS',
			'800.120.102' => 'ERROR_GENERAL_LIMIT_TRANSACTIONS',
			'800.120.103' => 'ERROR_GENERAL_LIMIT_TRANSACTIONS',
			'800.120.200' => 'ERROR_GENERAL_LIMIT_TRANSACTIONS',
			'800.120.201' => 'ERROR_GENERAL_LIMIT_TRANSACTIONS',
			'800.120.202' => 'ERROR_GENERAL_LIMIT_TRANSACTIONS',
			'800.120.203' => 'ERROR_GENERAL_LIMIT_TRANSACTIONS',

			'800.100.152' => 'ERROR_CC_DECLINED_AUTH',
			'000.400.106' => 'ERROR_CC_DECLINED_AUTH',
			'000.400.105' => 'ERROR_CC_DECLINED_AUTH',
			'000.400.103' => 'ERROR_CC_DECLINED_AUTH',

			'100.380.501' => 'ERROR_GENERAL_DECLINED_RISK',

			'800.400.151' => 'ERROR_CC_ADDRESS',
			'800.400.150' => 'ERROR_CC_ADDRESS',

			'100.400.300' => 'ERROR_GENERAL_CANCEL',
			'100.396.101' => 'ERROR_GENERAL_CANCEL',
			'900.300.600' => 'ERROR_GENERAL_CANCEL',

			'800.100.501' => 'ERROR_CC_RECURRING',
			'800.100.500' => 'ERROR_CC_RECURRING',

			'800.100.178' => 'ERROR_CC_REPEATED',
			'800.300.500' => 'ERROR_CC_REPEATED',
			'800.300.501' => 'ERROR_CC_REPEATED',

			'800.700.101' => 'ERROR_GENERAL_ADDRESS',
			'800.700.201' => 'ERROR_GENERAL_ADDRESS',
			'800.700.500' => 'ERROR_GENERAL_ADDRESS',
			'800.800.102' => 'ERROR_GENERAL_ADDRESS',
			'800.800.202' => 'ERROR_GENERAL_ADDRESS',
			'800.800.302' => 'ERROR_GENERAL_ADDRESS',
			'800.900.101' => 'ERROR_GENERAL_ADDRESS',
			'800.100.198' => 'ERROR_GENERAL_ADDRESS',
			'000.100.201' => 'ERROR_GENERAL_ADDRESS',

			'100.400.121' => 'ERROR_GENERAL_BLACKLIST',
			'800.100.172' => 'ERROR_GENERAL_BLACKLIST',
			'800.200.159' => 'ERROR_GENERAL_BLACKLIST',
			'800.200.160' => 'ERROR_GENERAL_BLACKLIST',
			'800.200.165' => 'ERROR_GENERAL_BLACKLIST',
			'800.200.202' => 'ERROR_GENERAL_BLACKLIST',
			'800.200.208' => 'ERROR_GENERAL_BLACKLIST',
			'800.200.220' => 'ERROR_GENERAL_BLACKLIST',
			'800.300.101' => 'ERROR_GENERAL_BLACKLIST',
			'800.300.102' => 'ERROR_GENERAL_BLACKLIST',
			'800.300.200' => 'ERROR_GENERAL_BLACKLIST',
			'800.300.301' => 'ERROR_GENERAL_BLACKLIST',
			'800.300.302' => 'ERROR_GENERAL_BLACKLIST',

			'000.100.200' => 'ERROR_GENERAL_GENERAL',
			'000.100.202' => 'ERROR_GENERAL_GENERAL',
			'000.100.206' => 'ERROR_GENERAL_GENERAL',
			'000.100.207' => 'ERROR_GENERAL_GENERAL',
			'000.100.208' => 'ERROR_GENERAL_GENERAL',
			'000.100.209' => 'ERROR_GENERAL_GENERAL',
			'000.100.210' => 'ERROR_GENERAL_GENERAL',
			'000.100.220' => 'ERROR_GENERAL_GENERAL',
			'000.100.221' => 'ERROR_GENERAL_GENERAL',
			'000.100.222' => 'ERROR_GENERAL_GENERAL',
			'000.100.223' => 'ERROR_GENERAL_GENERAL',
			'000.100.224' => 'ERROR_GENERAL_GENERAL',
			'000.100.225' => 'ERROR_GENERAL_GENERAL',
			'000.100.226' => 'ERROR_GENERAL_GENERAL',
			'000.100.227' => 'ERROR_GENERAL_GENERAL',
			'000.100.228' => 'ERROR_GENERAL_GENERAL',
			'000.100.229' => 'ERROR_GENERAL_GENERAL',
			'000.100.230' => 'ERROR_GENERAL_GENERAL',
			'000.100.299' => 'ERROR_GENERAL_GENERAL',
			'000.400.102' => 'ERROR_GENERAL_GENERAL',
			'000.400.200' => 'ERROR_GENERAL_GENERAL',
			'100.211.105' => 'ERROR_GENERAL_GENERAL',
			'100.211.106' => 'ERROR_GENERAL_GENERAL',
			'100.212.101' => 'ERROR_GENERAL_GENERAL',
			'100.212.102' => 'ERROR_GENERAL_GENERAL',
			'100.212.103' => 'ERROR_GENERAL_GENERAL',
			'100.250.100' => 'ERROR_GENERAL_GENERAL',
			'100.370.100' => 'ERROR_GENERAL_GENERAL',
			'100.380.100' => 'ERROR_GENERAL_GENERAL',
			'100.390.110' => 'ERROR_GENERAL_GENERAL',
			'100.390.113' => 'ERROR_GENERAL_GENERAL',
			'100.395.501' => 'ERROR_GENERAL_GENERAL',
			'100.396.102' => 'ERROR_GENERAL_GENERAL',
			'100.396.103' => 'ERROR_GENERAL_GENERAL',
			'100.396.104' => 'ERROR_GENERAL_GENERAL',
			'100.396.106' => 'ERROR_GENERAL_GENERAL',
			'100.396.201' => 'ERROR_GENERAL_GENERAL',
			'100.397.101' => 'ERROR_GENERAL_GENERAL',
			'100.397.102' => 'ERROR_GENERAL_GENERAL',
			'100.400.007' => 'ERROR_GENERAL_GENERAL',
			'100.400.041' => 'ERROR_GENERAL_GENERAL',
			'100.400.042' => 'ERROR_GENERAL_GENERAL',
			'100.400.043' => 'ERROR_GENERAL_GENERAL',
			'100.400.044' => 'ERROR_GENERAL_GENERAL',
			'100.400.045' => 'ERROR_GENERAL_GENERAL',
			'100.400.051' => 'ERROR_GENERAL_GENERAL',
			'100.400.061' => 'ERROR_GENERAL_GENERAL',
			'100.400.063' => 'ERROR_GENERAL_GENERAL',
			'100.400.064' => 'ERROR_GENERAL_GENERAL',
			'100.400.065' => 'ERROR_GENERAL_GENERAL',
			'100.400.071' => 'ERROR_GENERAL_GENERAL',
			'100.400.083' => 'ERROR_GENERAL_GENERAL',
			'100.400.084' => 'ERROR_GENERAL_GENERAL',
			'100.400.085' => 'ERROR_GENERAL_GENERAL',
			'100.400.091' => 'ERROR_GENERAL_GENERAL',
			'100.400.122' => 'ERROR_GENERAL_GENERAL',
			'100.400.130' => 'ERROR_GENERAL_GENERAL',
			'100.400.139' => 'ERROR_GENERAL_GENERAL',
			'100.400.140' => 'ERROR_GENERAL_GENERAL',
			'100.400.243' => 'ERROR_GENERAL_GENERAL',
			'100.400.301' => 'ERROR_GENERAL_GENERAL',
			'100.400.303' => 'ERROR_GENERAL_GENERAL',
			'100.400.304' => 'ERROR_GENERAL_GENERAL',
			'100.400.305' => 'ERROR_GENERAL_GENERAL',
			'100.400.306' => 'ERROR_GENERAL_GENERAL',
			'100.400.307' => 'ERROR_GENERAL_GENERAL',
			'100.400.308' => 'ERROR_GENERAL_GENERAL',
			'100.400.309' => 'ERROR_GENERAL_GENERAL',
			'100.400.310' => 'ERROR_GENERAL_GENERAL',
			'100.400.311' => 'ERROR_GENERAL_GENERAL',
			'100.400.312' => 'ERROR_GENERAL_GENERAL',
			'100.400.313' => 'ERROR_GENERAL_GENERAL',
			'100.400.314' => 'ERROR_GENERAL_GENERAL',
			'100.400.315' => 'ERROR_GENERAL_GENERAL',
			'100.400.318' => 'ERROR_GENERAL_GENERAL',
			'100.400.320' => 'ERROR_GENERAL_GENERAL',
			'100.400.321' => 'ERROR_GENERAL_GENERAL',
			'100.400.322' => 'ERROR_GENERAL_GENERAL',
			'100.400.323' => 'ERROR_GENERAL_GENERAL',
			'100.400.324' => 'ERROR_GENERAL_GENERAL',
			'100.400.325' => 'ERROR_GENERAL_GENERAL',
			'100.400.326' => 'ERROR_GENERAL_GENERAL',
			'100.400.327' => 'ERROR_GENERAL_GENERAL',
			'100.400.328' => 'ERROR_GENERAL_GENERAL',
			'100.400.500' => 'ERROR_GENERAL_GENERAL',
			'100.500.101' => 'ERROR_GENERAL_GENERAL',
			'100.500.201' => 'ERROR_GENERAL_GENERAL',
			'100.500.301' => 'ERROR_GENERAL_GENERAL',
			'100.500.302' => 'ERROR_GENERAL_GENERAL',
			'100.550.300' => 'ERROR_GENERAL_GENERAL',
			'100.550.301' => 'ERROR_GENERAL_GENERAL',
			'100.550.303' => 'ERROR_GENERAL_GENERAL',
			'100.550.312' => 'ERROR_GENERAL_GENERAL',
			'100.550.400' => 'ERROR_GENERAL_GENERAL',
			'100.550.401' => 'ERROR_GENERAL_GENERAL',
			'100.550.601' => 'ERROR_GENERAL_GENERAL',
			'100.550.603' => 'ERROR_GENERAL_GENERAL',
			'100.550.605' => 'ERROR_GENERAL_GENERAL',
			'100.600.500' => 'ERROR_GENERAL_GENERAL',
			'100.700.100' => 'ERROR_GENERAL_GENERAL',
			'100.700.101' => 'ERROR_GENERAL_GENERAL',
			'100.700.200' => 'ERROR_GENERAL_GENERAL',
			'100.700.201' => 'ERROR_GENERAL_GENERAL',
			'100.700.300' => 'ERROR_GENERAL_GENERAL',
			'100.700.400' => 'ERROR_GENERAL_GENERAL',
			'100.700.500' => 'ERROR_GENERAL_GENERAL',
			'100.700.800' => 'ERROR_GENERAL_GENERAL',
			'100.700.801' => 'ERROR_GENERAL_GENERAL',
			'100.700.802' => 'ERROR_GENERAL_GENERAL',
			'100.700.810' => 'ERROR_GENERAL_GENERAL',
			'100.800.100' => 'ERROR_GENERAL_GENERAL',
			'100.800.101' => 'ERROR_GENERAL_GENERAL',
			'100.800.102' => 'ERROR_GENERAL_GENERAL',
			'100.800.200' => 'ERROR_GENERAL_GENERAL',
			'100.800.201' => 'ERROR_GENERAL_GENERAL',
			'100.800.202' => 'ERROR_GENERAL_GENERAL',
			'100.800.300' => 'ERROR_GENERAL_GENERAL',
			'100.800.301' => 'ERROR_GENERAL_GENERAL',
			'100.800.302' => 'ERROR_GENERAL_GENERAL',
			'100.800.400' => 'ERROR_GENERAL_GENERAL',
			'100.800.401' => 'ERROR_GENERAL_GENERAL',
			'100.800.500' => 'ERROR_GENERAL_GENERAL',
			'100.800.501' => 'ERROR_GENERAL_GENERAL',
			'100.900.100' => 'ERROR_GENERAL_GENERAL',
			'100.900.101' => 'ERROR_GENERAL_GENERAL',
			'100.900.105' => 'ERROR_GENERAL_GENERAL',
			'100.900.200' => 'ERROR_GENERAL_GENERAL',
			'100.900.300' => 'ERROR_GENERAL_GENERAL',
			'100.900.301' => 'ERROR_GENERAL_GENERAL',
			'100.900.400' => 'ERROR_GENERAL_GENERAL',
			'100.900.401' => 'ERROR_GENERAL_GENERAL',
			'100.900.450' => 'ERROR_GENERAL_GENERAL',
			'100.900.500' => 'ERROR_GENERAL_GENERAL',
			'200.100.101' => 'ERROR_GENERAL_GENERAL',
			'200.100.102' => 'ERROR_GENERAL_GENERAL',
			'200.100.103' => 'ERROR_GENERAL_GENERAL',
			'200.100.150' => 'ERROR_GENERAL_GENERAL',
			'200.100.151' => 'ERROR_GENERAL_GENERAL',
			'200.100.199' => 'ERROR_GENERAL_GENERAL',
			'200.100.201' => 'ERROR_GENERAL_GENERAL',
			'200.100.300' => 'ERROR_GENERAL_GENERAL',
			'200.100.301' => 'ERROR_GENERAL_GENERAL',
			'200.100.302' => 'ERROR_GENERAL_GENERAL',
			'200.100.401' => 'ERROR_GENERAL_GENERAL',
			'200.100.402' => 'ERROR_GENERAL_GENERAL',
			'200.100.403' => 'ERROR_GENERAL_GENERAL',
			'200.100.404' => 'ERROR_GENERAL_GENERAL',
			'200.100.501' => 'ERROR_GENERAL_GENERAL',
			'200.100.502' => 'ERROR_GENERAL_GENERAL',
			'200.100.503' => 'ERROR_GENERAL_GENERAL',
			'200.100.504' => 'ERROR_GENERAL_GENERAL',
			'200.200.106' => 'ERROR_GENERAL_GENERAL',
			'200.300.403' => 'ERROR_GENERAL_GENERAL',
			'200.300.404' => 'ERROR_GENERAL_GENERAL',
			'200.300.405' => 'ERROR_GENERAL_GENERAL',
			'200.300.406' => 'ERROR_GENERAL_GENERAL',
			'200.300.407' => 'ERROR_GENERAL_GENERAL',
			'500.100.201' => 'ERROR_GENERAL_GENERAL',
			'500.100.202' => 'ERROR_GENERAL_GENERAL',
			'500.100.203' => 'ERROR_GENERAL_GENERAL',
			'500.100.301' => 'ERROR_GENERAL_GENERAL',
			'500.100.302' => 'ERROR_GENERAL_GENERAL',
			'500.100.303' => 'ERROR_GENERAL_GENERAL',
			'500.100.304' => 'ERROR_GENERAL_GENERAL',
			'500.100.401' => 'ERROR_GENERAL_GENERAL',
			'500.100.402' => 'ERROR_GENERAL_GENERAL',
			'500.100.403' => 'ERROR_GENERAL_GENERAL',
			'500.200.101' => 'ERROR_GENERAL_GENERAL',
			'600.100.100' => 'ERROR_GENERAL_GENERAL',
			'600.200.100' => 'ERROR_GENERAL_GENERAL',
			'600.200.200' => 'ERROR_GENERAL_GENERAL',
			'600.200.201' => 'ERROR_GENERAL_GENERAL',
			'600.200.202' => 'ERROR_GENERAL_GENERAL',
			'600.200.300' => 'ERROR_GENERAL_GENERAL',
			'600.200.310' => 'ERROR_GENERAL_GENERAL',
			'600.200.400' => 'ERROR_GENERAL_GENERAL',
			'600.200.500' => 'ERROR_GENERAL_GENERAL',
			'600.200.600' => 'ERROR_GENERAL_GENERAL',
			'600.200.700' => 'ERROR_GENERAL_GENERAL',
			'600.200.800' => 'ERROR_GENERAL_GENERAL',
			'600.200.810' => 'ERROR_GENERAL_GENERAL',
			'700.100.100' => 'ERROR_GENERAL_GENERAL',
			'700.100.200' => 'ERROR_GENERAL_GENERAL',
			'700.100.300' => 'ERROR_GENERAL_GENERAL',
			'700.100.400' => 'ERROR_GENERAL_GENERAL',
			'700.100.500' => 'ERROR_GENERAL_GENERAL',
			'700.100.600' => 'ERROR_GENERAL_GENERAL',
			'700.100.700' => 'ERROR_GENERAL_GENERAL',
			'700.100.701' => 'ERROR_GENERAL_GENERAL',
			'700.100.710' => 'ERROR_GENERAL_GENERAL',
			'700.300.500' => 'ERROR_GENERAL_GENERAL',
			'700.400.000' => 'ERROR_GENERAL_GENERAL',
			'700.400.400' => 'ERROR_GENERAL_GENERAL',
			'700.400.402' => 'ERROR_GENERAL_GENERAL',
			'700.400.410' => 'ERROR_GENERAL_GENERAL',
			'700.400.420' => 'ERROR_GENERAL_GENERAL',
			'700.400.562' => 'ERROR_GENERAL_GENERAL',
			'700.400.570' => 'ERROR_GENERAL_GENERAL',
			'700.400.700' => 'ERROR_GENERAL_GENERAL',
			'700.450.001' => 'ERROR_GENERAL_GENERAL',
			'800.100.100' => 'ERROR_GENERAL_GENERAL',
			'800.100.150' => 'ERROR_GENERAL_GENERAL',
			'800.100.179' => 'ERROR_GENERAL_GENERAL',
			'800.110.100' => 'ERROR_GENERAL_GENERAL',
			'800.120.300' => 'ERROR_GENERAL_GENERAL',
			'800.120.401' => 'ERROR_GENERAL_GENERAL',
			'800.121.100' => 'ERROR_GENERAL_GENERAL',
			'800.130.100' => 'ERROR_GENERAL_GENERAL',
			'800.140.100' => 'ERROR_GENERAL_GENERAL',
			'800.140.101' => 'ERROR_GENERAL_GENERAL',
			'800.140.110' => 'ERROR_GENERAL_GENERAL',
			'800.140.111' => 'ERROR_GENERAL_GENERAL',
			'800.140.112' => 'ERROR_GENERAL_GENERAL',
			'800.140.113' => 'ERROR_GENERAL_GENERAL',
			'800.160.100' => 'ERROR_GENERAL_GENERAL',
			'800.160.110' => 'ERROR_GENERAL_GENERAL',
			'800.160.120' => 'ERROR_GENERAL_GENERAL',
			'800.160.130' => 'ERROR_GENERAL_GENERAL',
			'800.400.100' => 'ERROR_GENERAL_GENERAL',
			'800.400.101' => 'ERROR_GENERAL_GENERAL',
			'800.400.102' => 'ERROR_GENERAL_GENERAL',
			'800.400.103' => 'ERROR_GENERAL_GENERAL',
			'800.400.104' => 'ERROR_GENERAL_GENERAL',
			'800.400.105' => 'ERROR_GENERAL_GENERAL',
			'800.400.110' => 'ERROR_GENERAL_GENERAL',
			'800.400.500' => 'ERROR_GENERAL_GENERAL',
			'800.500.100' => 'ERROR_GENERAL_GENERAL',
			'800.500.110' => 'ERROR_GENERAL_GENERAL',
			'800.600.100' => 'ERROR_GENERAL_GENERAL',
			'800.700.100' => 'ERROR_GENERAL_GENERAL',
			'800.800.800' => 'ERROR_GENERAL_GENERAL',
			'800.800.801' => 'ERROR_GENERAL_GENERAL',
			'800.900.100' => 'ERROR_GENERAL_GENERAL',
			'800.900.201' => 'ERROR_GENERAL_GENERAL',
			'800.900.303' => 'ERROR_GENERAL_GENERAL',
			'800.900.401' => 'ERROR_GENERAL_GENERAL',
			'900.100.100' => 'ERROR_GENERAL_GENERAL',
			'900.100.200' => 'ERROR_GENERAL_GENERAL',
			'900.100.201' => 'ERROR_GENERAL_GENERAL',
			'900.100.202' => 'ERROR_GENERAL_GENERAL',
			'900.100.203' => 'ERROR_GENERAL_GENERAL',
			'900.100.300' => 'ERROR_GENERAL_GENERAL',
			'900.100.400' => 'ERROR_GENERAL_GENERAL',
			'900.100.500' => 'ERROR_GENERAL_GENERAL',
			'900.100.600' => 'ERROR_GENERAL_GENERAL',
			'900.200.100' => 'ERROR_GENERAL_GENERAL',
			'900.400.100' => 'ERROR_GENERAL_GENERAL',
			'999.999.999' => 'ERROR_GENERAL_GENERAL',

			'000.400.107' => 'ERROR_GENERAL_TIMEOUT',
			'100.395.502' => 'ERROR_GENERAL_TIMEOUT',

			'700.400.100' => 'ERROR_CAPTURE_BACKEND',
			'700.400.101' => 'ERROR_CAPTURE_BACKEND',
			'700.400.510' => 'ERROR_CAPTURE_BACKEND',

			'800.100.500' => 'ERROR_REORDER_BACKEND',
			'800.100.501' => 'ERROR_REORDER_BACKEND',

			'700.300.300' => 'ERROR_REFUND_BACKEND',
			'700.300.400' => 'ERROR_REFUND_BACKEND',
			'700.300.600' => 'ERROR_REFUND_BACKEND',
			'700.300.700' => 'ERROR_REFUND_BACKEND',
			'700.400.200' => 'ERROR_REFUND_BACKEND',
			'700.400.300' => 'ERROR_REFUND_BACKEND',
			'700.400.520' => 'ERROR_REFUND_BACKEND',
			'700.400.530' => 'ERROR_REFUND_BACKEND',
			'700.300.100' => 'ERROR_REFUND_BACKEND',

			'700.400.560' => 'ERROR_RECEIPT_BACKEND',
			'700.400.561' => 'ERROR_RECEIPT_BACKEND',

			'800.900.200' => 'ERROR_ADDRESS_PHONE'
		);
		if ($code) {
			return array_key_exists($code, $error_messages) ? $error_messages[$code] : 'ERROR_UNKNOWN';
		} else {
			return 'ERROR_UNKNOWN';
		}
	}

	/**
	 * Get error identifier for back office operations
	 *
	 * @param string $code
	 * @return string
	 */
	public static function getErrorIdentifierBackend($code)
	{
		$error_messages = array(
			'700.400.100' => 'ERROR_CAPTURE_BACKEND',
			'700.400.101' => 'ERROR_CAPTURE_BACKEND',
			'700.400.510' => 'ERROR_CAPTURE_BACKEND',

			'800.100.500' => 'ERROR_REORDER_BACKEND',
			'800.100.501' => 'ERROR_REORDER_BACKEND',

			'700.300.300' => 'ERROR_REFUND_BACKEND',
			'700.300.400' => 'ERROR_REFUND_BACKEND',
			'700.300.600' => 'ERROR_REFUND_BACKEND',
			'700.300.700' => 'ERROR_REFUND_BACKEND',
			'700.400.200' => 'ERROR_REFUND_BACKEND',
			'700.400.300' => 'ERROR_REFUND_BACKEND',
			'700.400.520' => 'ERROR_REFUND_BACKEND',
			'700.400.530' => 'ERROR_REFUND_BACKEND',
			'700.300.100' => 'ERROR_REFUND_BACKEND',

			'700.400.560' => 'ERROR_RECEIPT_BACKEND',
			'700.400.561' => 'ERROR_RECEIPT_BACKEND'
		);
		if ($code) {
			return array_key_exists($code, $error_messages) ? $error_messages[$code] : 'ERROR_GENERAL_PROCESSING';
		} else {
			return 'ERROR_GENERAL_PROCESSING';
		}
	}

	/**
	 * Check if response code from gateway is 'in review'
	 *
	 * @param string|boolean $code
	 * @return boolean
	 */
	public static function isSuccessReview($code)
	{
		$in_reviews = array(
			'000.400.000',
			'000.400.010',
			'000.400.020',
			'000.400.030',
			'000.400.040',
			'000.400.050',
			'000.400.060',
			'000.400.070',
			'000.400.080',
			'000.400.090'
		);
		if (in_array($code, $in_reviews)) {
			return true;
		} else {
			return false;
		}
	}
}
