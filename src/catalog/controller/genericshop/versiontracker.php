<?php
/**
 * VersionTracker class to to share IP, email address, etc with Genericshop.
 *
 */
class VersionTracker
{
	private static $version_tracker_url = 'http://api.dbserver.payreto.eu/v1/tracker';

	/**
	 * Get the version tracker URL
	 *
	 * @return  string
	 */
	private static function getVersionTrackerUrl()
	{
		return self::$version_tracker_url;
	}

	/**
	 * Get response data from the API
	 *
	 * @param   array  $data
	 * @param   string  $url
	 * @return  boolean|string
	 */
	private static function getResponseData($data, $url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$response = curl_exec($ch);
		if (curl_errno($ch)) {
			return false;
		}
		curl_close($ch);
		return json_decode($response, true);
	}

	/**
	 * Get the version tracker parameter
	 *
	 * @param   array  $version_data
	 * @return  string
	 */
	private static function getVersionTrackerParameter($version_data)
	{
		$data = 'transaction_mode=' . $version_data['transaction_mode'] .
				'&ip_address=' . $version_data['ip_address'] .
				'&shop_version=' . $version_data['shop_version'] .
				'&plugin_version=' . $version_data['plugin_version'] .
				'&client=' . $version_data['client'] .
				'&hash=' . md5($version_data['shop_version'] . $version_data['plugin_version'] . $version_data['client']);

		if ($version_data['shop_system']) {
			$data .= '&shop_system=' . $version_data['shop_system'];
		}
		if ($version_data['email']) {
			$data .= '&email=' . $version_data['email'];
		}
		if ($version_data['merchant_id']) {
			$data .= '&merchant_id=' . $version_data['merchant_id'];
		}
		if ($version_data['shop_url']) {
			$data .= '&shop_url=' . $version_data['shop_url'];
		}
		if ($version_data['merchant_location']) {
			$data .= '&merchant_location=' . $version_data['merchant_location'];
		}

		return $data;
	}

	/**
	 * Send the version tracker data into the API
	 *
	 * @param   array  $version_data
	 * @return  string
	 */
	public static function sendVersionTracker($version_data)
	{
		$post_data = self::getVersionTrackerParameter($version_data);
		$url = self::getVersionTrackerUrl();
		return self::getResponseData($post_data, $url);
	}
}
