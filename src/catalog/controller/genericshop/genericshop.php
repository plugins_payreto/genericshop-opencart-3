<?php

/* Generic Shop module
 *
 * @version 3.0.0
 * @date 2018-04-25
 *
 */

include_once(dirname(__FILE__) . '/core.php');
include_once(dirname(__FILE__) . '/versiontracker.php');

class ControllerGenericshop extends Controller
{
	protected $version = '3.0.0';
	protected $code='';
	protected $client = 'GenericShop';
	protected $shop_system = 'Opencart';
	protected $shop_version = VERSION;
	protected $recurring = false;
	protected $account_type = '';
	protected $group_recurring = '';
	protected $redirect = false;
	protected $test_mode = 'EXTERNAL';
	protected $template = 'extension/payment/genericshop/form_cp';
	protected $payment_type = 'DB';
	protected $brand = '';
	protected $logo = '';

	/**
	 * this function is the constructor of ControllerGenericshop class
	 *
	 * @return  void
	 */
	public function index()
	{
		// Validate cart has products and has stock.
		if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
			$this->response->redirect($this->url->link('checkout/cart'));
		}

		// Validate minimum quantity requirments.
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}
			if ($product['minimum'] > $product_total) {
				$this->response->redirect($this->url->link('checkout/cart'));
			}
		}

		$this->language->load('extension/payment/genericshop');

		$data['FRONTEND_TT_TESTMODE'] = $this->language->get('FRONTEND_TT_TESTMODE');
		$data['FRONTEND_BT_CANCEL'] = $this->language->get('FRONTEND_BT_CANCEL');
		$data['FRONTEND_BT_CONFIRM'] = $this->language->get('FRONTEND_BT_CONFIRM');
		$data['FRONTEND_BT_PAYNOW'] = $this->language->get('FRONTEND_BT_PAYNOW');
		$data['FRONTEND_MC_PAYANDSAFE'] = $this->language->get('FRONTEND_MC_PAYANDSAFE');
		$data['FRONTEND_RECURRING_WIDGET_HEADER1'] = $this->language->get('FRONTEND_RECURRING_WIDGET_HEADER1');
		$data['FRONTEND_RECURRING_WIDGET_HEADER2'] = $this->language->get('FRONTEND_RECURRING_WIDGET_HEADER2');
		$data['FRONTEND_MERCHANT_LOCATION_DESC'] = $this->language->get('FRONTEND_MERCHANT_LOCATION_DESC');

		$merchant_location = $this->getMerchantLocation();

		$data['lang'] = $this->getLangCode();
		$data['brand'] = $this->getBrand();
		$data['redirect'] = $this->isRedirect();
		$data['recurring'] = $this->isRecurring();
		$data['cancel_url'] = $this->url->link('checkout/checkout', '', true);
		$data['response_url'] = $this->url->link('extension/payment/' . $this->code . '/callback', '', true);
		$data['test_mode'] = $this->getTestMode();
		$data['registrations'] = $this->getPaymentAccount();
		$data['payment_widget_url'] = $this->getPaymentWidgetUrl();
		$data['merchant_location'] = $merchant_location;

		$this->language->load('checkout/checkout');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_cart'),
			'href'      => $this->url->link('checkout/cart')
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('checkout/checkout', '', true)
		);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$data['config_template'] =  $this->config->get('config_template');
		$data['payment_form_css'] = $this->isPaymentFormCssExist($this->config->get('config_template'));

		$this->response->setOutput($this->load->view($this->getTemplate(), $data));
	}

	/**
	 * Get merchant location from admin configuration
	 *
	 * @return  array
	 */
	function getMerchantLocation() {
		return $this->config->get('payment_genericshop_ageneral_merchant_location');
	}

	/**
	 * return true if payment form css is exist on path that already defined in backend configuration
	 *
	 * @param string $config_template
	 * @return  boolean
	 */
	public function isPaymentFormCssExist($config_template){
		if (file_exists('../../view/theme/' . $config_template . '/genericshop/payment_form.css')) {
			return true;
		}
		return false;
	}

	/**
	 * Set attributes variable base on payment method name
	 *
	 * @param string $payment_method
	 * @return  void
	 */
	public function setPaymentMethodAtrributes($payment_method)
	{
		$this->code = $payment_method;
		switch ($this->code) {
			case 'genericshop_cc_saved':
				$this->brand = "VISA MASTER AMEX DINERS JCB";
				$this->brand = $this->getBrandCards();
				$this->group_recurring = 'CC';
				$this->payment_type = $this->getPaymentTypeSelection();
				break;
		}
	}

	/**
	 * To load the confirm view
	 *
	 * @return  void
	 */
	public function confirmHtml()
	{
		$this->language->load('extension/payment/genericshop');

		$data['button_confirm'] = $this->language->get('button_confirm');
		$data['action'] = $this->url->link('checkout/' . $this->code, '', true);

		return $this->load->view('extension/payment/genericshop/confirm', $data);
	}

	/**
	 * redirect to the error message page or the failed message page
	 *
	 * @param string $error_identifier
	 * @return  void
	 */
	public function redirectError($error_identifer)
	{
		$this->language->load('extension/payment/genericshop');
		$this->session->data['error'] = $this->language->get($error_identifer);
		$this->response->redirect($this->url->link('checkout/checkout', '', true));
	}

	/**
	 * redirect to the error message page or the failed message page on my payment information page
	 *
	 * @param string $error_identifier
	 * @param string $recurring_id
	 * @return  void
	 */
	public function redirectErrorRecurring($error_identifer, $recurring_id)
	{
		$this->language->load('extension/payment/genericshop');
		$this->session->data['error'] = $this->language->get($error_identifer);
		if ($recurring_id) {
			$this->response->redirect($this->url->link('account/change_payment', 'selected_payment=' . $this->code . '&id=' . $recurring_id, true));
		} else {
			$this->response->redirect($this->url->link('account/register_payment', 'selected_payment=' . $this->code, true));
		}
	}

	/**
	 * Get languages code
	 *
	 * @return string
	 */
	function getLangCode()
	{
		switch (substr($this->session->data['language'], 0, 2)) {
			case 'de':
				$lang_code = "de";
				break;
			default:
				$lang_code = "en";
				break;
		}
		return $lang_code;
	}

	/**
	 * Get the payment type selection
	 *
	 * @return  string
	 */
	function getPaymentTypeSelection()
	{
		return $this->config->get('payment_' . $this->code . '_transaction_mode');
	}

	/**
	 * Get a payment type
	 *
	 * @return  string
	 */
	function getPaymentType()
	{
		return $this->payment_type;
	}

	/**
	 * Get the payment is partial
	 *
	 * @return  string
	 */
	function getPaymentIsPartial()
	{
		return $this->config->get('payment_' . $this->code . '_payment_is_partial');
	}

	/**
	 * Get the merchant data
	 *
	 * @return  array
	 */
	function getMerchantData()
	{
		return array(
			'merchant_email' => $this->config->get('payment_genericshop_ageneral_merchant_email'),
			'merchant_no' => $this->config->get('payment_genericshop_ageneral_merchant_no'),
			'shop_url' => $this->config->get('payment_genericshop_ageneral_shop_url')
		);
	}

	/**
	 * Get a customer ip
	 *
	 * @return  string
	 */
	function getCustomerIp()
	{
		if ($_SERVER['REMOTE_ADDR'] == '::1') {
			return "127.0.0.1";
		}
		return $_SERVER['REMOTE_ADDR'];
	}

	/**
	 * Get a test mode
	 *
	 * @return  boolean
	 */
	function getTestMode()
	{
		if ($this->getServerMode() == "LIVE") {
			return false;
		} else {
			return $this->test_mode;
		}
	}

	/**
	 * Get a customer data
	 *
	 * @return  array
	 */
	function getCustomerData()
	{
		if ($this->isCustomerLogin() == 'true') {
			$this->load->model('genericshop/genericshop');
			$customer = array ();
			$customer_query = $this->model_genericshop_genericshop->getCustomerData();
			foreach ($customer_query as $row) {
				$customer[] = $row;
			}
			return $customer[0];
		}
	}

	/**
	 * Get a customer address
	 *
	 * @return  array
	 */
	function getCustomerAddress()
	{
		$this->load->model('genericshop/genericshop');
		$addresses = array ();
		$addresses_query = $this->model_genericshop_genericshop->getCustomerAddress();
		foreach ($addresses_query as $row) {
			$addresses[] = $row;
		}
		if(!empty($addresses)) {
			return $addresses[0];
		} else {
			$this->session->data['error'] = $this->language->get('ERROR_GENERAL_ADDRESS');
			$this->response->redirect($this->url->link('account/payment_information', '', true));
		}
	}

	/**
	 * Get a country code 2 based on the country id
	 *
	 * @param   string  $country_id
	 * @return  string
	 */
	function getCountryCode2($country_id)
	{
		$this->load->model('genericshop/genericshop');
		$country = $this->model_genericshop_genericshop->getCountryCode($country_id);
		return $country;
	}

	/**
	 * Get a server mode of payment methods
	 *
	 * @return  string
	 */
	function getServerMode()
	{
		return $this->config->get('payment_' . $this->code . '_server');
	}

	/**
	 * check the recurring activation of payment methods
	 *
	 * @return  boolean
	 */
	function isRecurring()
	{
		return $this->recurring;
	}

	/**
	 * Get the group of recurring payment
	 *
	 * @return  string
	 */
	function getGroupRecurring()
	{
		return $this->group_recurring;
	}

	/**
	 * Get brands
	 *
	 * @return  string
	 */
	function getBrand()
	{
		return $this->brand;
	}

	/**
	 * Get payment methods brands
	 *
	 * @return  string
	 */
	function getBrandCards()
	{
		$brand = '';
		$brands = $this->config->get('payment_' . $this->code . '_cards_types');
		if (!$brands) {
			return $this->brand;
		} else {
			$brands = explode(',', $brands);
		}
		foreach ($brands as $value) {
			$brand .= $value . ' ';
		}
		return trim($brand);
	}

	/**
	 * Check a redirect mode of payment methods
	 *
	 * @return  boolean
	 */
	function isRedirect()
	{
		return $this->redirect;
	}

	/**
	 * Check the payment account if its has default setting
	 *
	 * @return  string
	 */
	function checkDefault()
	{
		$this->load->model('genericshop/genericshop');
		$count_row = $this->model_genericshop_genericshop->checkDefault($this->code);
		if ($count_row > 0) {
			return 0;
		}

		return 1;
	}

	/**
	 * Get a reference id
	 *
	 * @param   string  $recurring_id
	 * @return  string
	 */
	function getReferenceId($recurring_id)
	{
		$this->load->model('genericshop/genericshop');
		;
		$reference_id = $this->model_genericshop_genericshop->getReferenceId($recurring_id);

		return $reference_id;
	}

	/**
	 * Check availability the reference id
	 *
	 * @param   string  $reference_id
	 * @return  string
	 */
	function isRegistered($reference_id)
	{
		$this->load->model('genericshop/genericshop');
		$count_row = $this->model_genericshop_genericshop->checkReferenceId($reference_id);
		if ($count_row > 0) {
			return true;
		}

		return false;
	}

	/**
	 * Get payment accounts list
	 *
	 * @return  array
	 */
	function getPaymentAccount()
	{
		$this->load->model('genericshop/genericshop');

		return $this->model_genericshop_genericshop->getPaymentAccount($this->code);
	}

	/**
	 * Get a template
	 *
	 * @return  string
	 */
	function getTemplate()
	{
		return $this->template;
	}

	/**
	 * Check the multi channel activation of a payment methods
	 *
	 * @return  boolean
	 */
	function isMultiChannel()
	{
		return $this->config->get('payment_' . $this->code . '_multichannel');
	}

	/**
	 * Check the version tracker activation
	 *
	 * @return  string
	 */
	function isVersionTrackerActive()
	{
		return $this->config->get('payment_genericshop_ageneral_version_tracker');
	}

	/**
	 * Get a register amount of payment method recurring
	 *
	 * @return  string
	 */
	function getRegisterAmount()
	{
		return $this->config->get('payment_' . $this->code . '_amount');
	}

	/**
	 * Get 3d amount value
	 *
	 * @return  string
	 */
	function get3dAmount()
	{
		return $this->getRegisterAmount();
	}

	/**
	 * Get 3d currency from session
	 *
	 * @return  string
	 */
	function get3dCurrency()
	{
		return $this->session->data['currency'];
	}

	/**
	 * Get the account of payment methods recurring
	 *
	 * @param   array  $payment_status
	 * @return  array
	 */
	function getAccount($payment_status)
	{
		return $payment_status[$this->account_type];
	}

	/**
	 * Get the credentials of payment methods
	 *
	 * @return  array
	 */
	function getCredentials()
	{
		$credentials = array(
			'server_mode' => $this->config->get('payment_' . $this->code . '_server'),
			'channel_id'  => $this->config->get('payment_' . $this->code . '_channel_id'),
			'login'       => $this->config->get('payment_genericshop_ageneral_login'),
			'password'    => $this->config->get('payment_genericshop_ageneral_password')
		);

		if ($this->isMultiChannel()) {
			$credentials['channel_id_moto'] = $this->config->get('payment_' . $this->code . '_moto');
		}

		return $credentials;
	}

	/**
	 * Get a customer sex
	 *
	 * @param array $custom_field
	 * @return  string
	 */
	function getCustomerSex($custom_field)
	{
		$id = $this->config->get('payment_genericshop_ageneral_gender_customfield');
		$male_value = $this->config->get('payment_genericshop_ageneral_gender_malevalue');
		$female_value = $this->config->get('payment_genericshop_ageneral_gender_femalevalue');
		if (isset($custom_field[$id])) {
			switch ($custom_field[$id]) {
				case $male_value:
					return 'M';
					break;
				case $female_value:
					return 'F';
					break;
			}
		}
		return false;
	}

	/**
	 * Get the date of birth of a customer
	 *
	 * @param array $custom_field
	 * @return  string
	 */
	function getCustomerDob($custom_field)
	{
		$id = $this->config->get('payment_genericshop_ageneral_dob_customfield');
		if (isset($custom_field[$id])) {
			return $custom_field[$id];
		}
		return false;
	}

	/**
	 * Get checkout response
	 *
	 * @param string $payment_widget_url
	 * @param string $id
	 * @return  string
	 */
	function getCheckoutResultRecurring($order)
	{
		return GenericshopPaymentCore::getCheckoutResult($order);
	}

	/**
	 * Get order data
	 *
	 * @return  array
	 */
	function getOrderData($id = false)
	{
		$customer = $this->getCustomerData();
		$customer_address = $this->getCustomerAddress();

		$order = $this->getCredentials();
		$order['customer']['email'] = $customer['email'];
		$order['customer']['first_name'] = $customer['firstname'];
		$order['customer']['last_name'] = $customer['lastname'];
		$order['billing']['street'] = $customer_address['address_1'];
		$order['billing']['city'] = $customer_address['city'];
		$order['billing']['zip'] = $customer_address['postcode'];
		$order['billing']['country_code'] = $this->getCountryCode2($customer_address['country_id']);
		$order['amount'] = $this->getRegisterAmount();
		$order['currency'] = $this->session->data['currency'];
		$order['customer_ip'] = $this->getCustomerIp();

		if ($this->code == 'genericshop_cards_saved') {
			$order['3D']['amount'] = $this->get3dAmount();
			$order['3D']['currency'] = $this->get3dCurrency();
		}
		if ($this->getGroupRecurring() != 'VA') {
			$order['payment_type'] = $this->getPaymentType();
		}

		$order['test_mode'] = $this->getTestMode();
		$order['payment_recurring'] = 'INITIAL';
		$order['payment_registration'] = 'true';

		if ($id) {
			// change payment
			$order['transaction_id'] = $this->getReferenceId($id);
		} else {
			$order['transaction_id'] = (int)$this->customer->getId();
		}

		return $order;
	}

	/**
	 * Get cart items
	 *
	 * @return  array
	 */
	function getCartItems()
	{
		$this->load->model('account/order');
		$this->load->model('catalog/product');
		$this->load->model('genericshop/genericshop');

		$cart = $this->model_account_order->getOrderProducts($this->session->data['order_id']);
		$cart_items = array();
		$i = 0;
		foreach ($cart as $item) {
			$cart_items[$i]['merchant_item_id'] = (int)$item['product_id'];
			$cart_items[$i]['quantity'] = (int)$item['quantity'];
			$cart_items[$i]['name'] = $item['name'];

			$item_tax = (float)$item['tax'];
			$item_price = (float)$item['price'];

			$cart_items[$i]['price'] = $item_price + $item_tax;
			if ($item_tax > 0) {
				$cart_items[$i]['tax'] = $item_tax / $item_price * 100;
			}

			$product = $this->model_catalog_product->getProduct($item['product_id']);
			$discount_price = (float)$product['price'];
			$special_price = (float)$product['special'];
			$product_price = (float)$this->model_genericshop_genericshop->getProductPrice($item['product_id']);

			if ($special_price > 0 && $product_price > $special_price) {
				$cart_items[$i]['discount'] = ($product_price - $special_price) / $product_price * 100;
			} elseif ($discount_price > 0 && $product_price > $discount_price) {
				$cart_items[$i]['discount'] = ($product_price - $discount_price) / $product_price * 100;
			}
			$i = $i+1;
		}
		return $cart_items;
	}

	/**
	 * Get customer created date
	 *
	 * @return string|boolean
	 */
	protected function getCustomerCreatedDate()
	{
		if ($this->isCustomerLogin() == 'true') {
			$customer_id = (int)$this->customer->getId();
			$query = $this->db->query("SELECT date_added FROM `" . DB_PREFIX . "order` WHERE customer_id =" . $customer_id);
			$customer_date_added = date("Y-m-d", strtotime($query->row['date_added']));
			return $customer_date_added;
		}
		return date('Y-m-d');
	}

	/**
	 * Check customer login status and return as string
	 *
	 * @return string
	 */
	protected function isCustomerLogin()
	{
		if ($this->customer->isLogged()) {
			return 'true';
		}
		return 'false';
	}

	/**
	 * Get risk kunden status
	 *
	 * @return string|boolean
	 */
	protected function getRiskKundenStatus()
	{
		if ($this->getCustomerTotalOrdersByCompleteStatus() > 0) {
			return 'BESTANDSKUNDE';
		}
		return 'NEUKUNDE';
	}

	/**
	 * Get the customer total order
	 *
	 * @param array $order_info
	 * @return int
	 */
	public function getCustomerTotalOrdersByCompleteStatus()
	{
		if ($this->isCustomerLogin() == 'true') {
			$implode = array();
			$genericshop_complete_statuses = (int)$this->config->get('payment_genericshop_ageneral_accept_status_id');
			$order_statuses = $this->config->get('config_complete_status');
			$customer_id = (int)$this->customer->getId();

			foreach ($order_statuses as $order_status_id) {
				$implode[] = "order_status_id = '" . (int)$order_status_id . "'";
			}

			if ($implode) {
				$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE (" . implode(" OR ", $implode) . " OR order_status_id='" .
				$genericshop_complete_statuses . "') AND  customer_id=" . $customer_id);

				return (int)$query->row['total'];
			}
			return 0;
		}
		return 0;
	}

	/**
	 * Get the registration parameters
	 *
	 * @return  array
	 */
	function getRegistrationParameters()
	{
		$registration_parameters = array();
		if ($this->isRecurring()) {
			$registration_parameters['payment_recurring'] = 'INITIAL';
			$registration_parameters['payment_registration'] = 'true';

			$registrations = $this->getPaymentAccount();
			if (isset($registrations)) {
				foreach ($registrations as $key => $value) {
					$registration_parameters['registrations'][$key] = $value['ref_id'];
				}
			}
		}
		return $registration_parameters;
	}

	/**
	 * Get parameters on checkout
	 *
	 * @return  array
	 */
	public function getCheckoutParameters()
	{
		$order_id = $this->session->data['order_id'];
		$order_info = $this->model_checkout_order->getOrder($order_id);

		$checkout_parameters = $this->getCredentials();
		$checkout_parameters['customer']['email'] = $order_info['email'];
		$checkout_parameters['customer']['first_name'] = $order_info['payment_firstname'];
		$checkout_parameters['customer']['last_name'] = $order_info['payment_lastname'];
		$checkout_parameters['billing']['street'] = $order_info['payment_address_1'];
		$checkout_parameters['billing']['zip'] = $order_info['payment_postcode'];
		$checkout_parameters['billing']['city'] = $order_info['payment_city'];
		$checkout_parameters['billing']['country_code'] = $order_info['payment_iso_code_2'];
		$checkout_parameters['amount'] = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], false);
		$checkout_parameters['currency'] = $order_info['currency_code'];
		$checkout_parameters['customer_ip'] = $this->getCustomerIp();
		$checkout_parameters['test_mode'] = $this->getTestMode();
		$checkout_parameters['payment_type'] = $this->getPaymentType();
		$checkout_parameters['transaction_id'] = $order_info['order_id'];

		$checkout_parameters = array_merge_recursive(
			$checkout_parameters,
			$this->getRegistrationParameters()
		);

		return $checkout_parameters;
	}

	/**
	 * Get payment widget at checkout payment page
	 *
	 * @return  string
	 */
	public function getPaymentWidgetUrl()
	{
		$this->load->model('checkout/order');
		$order = $this->getCheckoutParameters();
		$this->session->data['genericshop']['order'] = $order;

		$checkout_result = GenericshopPaymentCore::getCheckoutResult($order);

		if ($checkout_result['is_valid']) {

			$checkout_id = $checkout_result['response']['id'];

			if (!isset($checkout_result['response']['id'])) {
				$this->redirectError('ERROR_GENERAL_REDIRECT');
			}

			$payment_widget_url = GenericshopPaymentCore::getPaymentWidgetUrl($order, $checkout_id);

			if (!$this->isPaymentWidgetValid($payment_widget_url)) {
				$this->redirectError('ERROR_GENERAL_REDIRECT');
			}

			return $payment_widget_url;
		} else {
			$this->redirectError($checkout_result['response']);
		}
	}

	/**
	* return true if doesn't find error in payment widget content
	*
	* @param string $payment_widget_url
	*
	* @return array
	*/
	public function isPaymentWidgetValid($payment_widget_url)
	{
		$payment_widget_content = GenericshopPaymentCore::getPaymentWidgetContent(
			$payment_widget_url,
			$this->getServerMode()
		);

		if (!$payment_widget_content['is_valid'] || strpos($payment_widget_content['response'], 'errorDetail') !== false) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Delete the payment account
	 *
	 * @param string $recurring_id
	 * @param string $type
	 * @return  string
	 */
	function deletePaymentAccount($recurring_id = false, $type = false)
	{
		$this->load->model('genericshop/genericshop');
		$reference_id = $this->getReferenceId($recurring_id);
		$order = $this->getCredentials();
		$order['transaction_id'] = (int)$this->customer->getId();
		$order['test_mode'] = $this->getTestMode();

		$delete_recurring_response = GenericshopPaymentCore::deleteRegisteredAccount($reference_id, $order);

		if (!$delete_recurring_response['is_valid']) {
			$this->session->data['error'] = $this->language->get($delete_recurring_response['response']);
			$this->response->redirect($this->url->link('account/payment_information', '', true));
		} else {
			$result_code = $delete_recurring_response["response"]["result"]["code"];
			$transaction_result = GenericshopPaymentCore::getTransactionResult($result_code);

			if ($transaction_result == "ACK") {
				$this->model_genericshop_genericshop->deletePaymentAccount($recurring_id);
			}

			return $transaction_result;
		}
	}

	/**
	 * Get the version data
	 *
	 * @return  array
	 */
	function getVersionData()
	{
		$merchant = $this->getMerchantData();
		$version_data['transaction_mode'] = $this->getServerMode();
		$version_data['ip_address'] = $_SERVER['SERVER_ADDR'];
		$version_data['shop_version'] = $this->shop_version;
		$version_data['plugin_version'] = $this->version;
		$version_data['client'] = $this->client;
		$version_data['email'] = $merchant['merchant_email'];
		$version_data['merchant_id'] = $merchant['merchant_no'];
		$version_data['shop_system'] = $this->shop_system;
		$version_data['shop_url'] = $merchant['shop_url'];
		return $version_data;
	}

	/**
	 * Get the payment account data
	 *
	 * @param string $registration_id
	 * @param array $payment_status
	 * @return  array
	 */
	function getPaymentAccountData($registration_id, $payment_status)
	{
		$payment_account_data = array();
		$account = $this->getAccount($payment_status);
		$credentials = $this->getCredentials();
		$default = $this->checkDefault();

		$payment_account_data['customer_id'] = (int)$this->customer->getId();
		$payment_account_data['group_recurring'] = $this->getGroupRecurring();
		$payment_account_data['payment_brand'] = $payment_status['paymentBrand'];
		$payment_account_data['email'] = isset($account['email']) ? $account['email'] : '';
		$payment_account_data['holder'] = isset($account['holder']) ? $account['holder'] : '';
		$payment_account_data['last4Digits'] = isset($account['last4Digits']) ? $account['last4Digits'] : '';
		$payment_account_data['expiryMonth'] = isset($account['expiryMonth']) ? $account['expiryMonth'] : '';
		$payment_account_data['expiryYear'] = isset($account['expiryYear']) ? $account['expiryYear'] : '';
		$payment_account_data['server_mode'] = $credentials['server_mode'];
		$payment_account_data['channel_id'] = $credentials['channel_id'];
		$payment_account_data['referenceId'] = $registration_id;
		$payment_account_data['default'] = $default;

		return $payment_account_data;
	}

	/**
	 * Insert the payment account data into the database
	 *
	 * @param string $registration_id
	 * @param array $payment_status
	 * @return  void
	 */
	function insertRegistration($registration_id, $payment_status)
	{
		$is_registered = $this->isRegistered($registration_id);

		if (!$is_registered) {
			$data = $this->getPaymentAccountData($registration_id, $payment_status);
			$this->load->model('genericshop/genericshop');
			$this->model_genericshop_genericshop->savePaymentAccount($data);
		}
	}

	/**
	 * Update the payment account data into the database
	 *
	 * @param string $recurring_id
	 * @param string $registration_id
	 * @param array $payment_status
	 * @return  void
	 */
	function updatePaymentAccount($recurring_id, $registration_id, $payment_status)
	{
		$data = $this->getPaymentAccountData($registration_id, $payment_status);
		$data['recurring_id'] = $recurring_id;

		$this->load->model('genericshop/genericshop');
		$this->model_genericshop_genericshop->updatePaymentAccount($data);
	}

	/**
	 * Insert or update the payment account into the database
	 *
	 * @param string $recurring_id
	 * @param string $registration_id
	 * @param array $payment_status
	 * @return  void
	 */
	function savePaymentAccount($recurring_id, $registration_id, $payment_status)
	{
		if ($recurring_id) {
			$this->updatePaymentAccount($recurring_id, $registration_id, $payment_status);
		} else {
			$this->insertRegistration($registration_id, $payment_status);
		}
	}

	/**
	 * Process Credit Cards recurring payment at my payment information page
	 *
	 * @param   string  $recurring_id
	 * @param   array  $payment_status
	 * @param   array  $order
	 * @return  void
	 */
	function processPaymentSavedRecurring($recurring_id, $payment_status, $order)
	{
		$registration_id = $payment_status['response']['registrationId'];
		$reference_id = $payment_status['response']['id'];

		if ($this->getPaymentType() == 'PA') {
			$order['payment_type'] = "CP";
			$response = GenericshopPaymentCore::backOfficeOperation($reference_id, $order);
			$result_code = $response["response"]["result"]["code"];
			$error_identifer = GenericshopPaymentCore::getErrorIdentifier($result_code);
			$transaction_result = GenericshopPaymentCore::getTransactionResult($result_code);

			if ($transaction_result == "ACK") {
				$reference_id = $response['response']['id'];
			} elseif ($transaction_result == "NOK") {
				$this->redirectErrorRecurring($error_identifer, $recurring_id);
			} else {
				$this->redirectErrorRecurring('ERROR_UNKNOWN', $recurring_id);
			}
		}
		$order['payment_type'] = "RF";
		GenericshopPaymentCore::backOfficeOperation($reference_id, $order);
		$this->savePaymentAccount($recurring_id, $registration_id, $payment_status['response']);
	}

	/**
	 * Process payment success at my payment information page
	 *
	 * @param   string  $recurring_id
	 * @param   array  $payment_status
	 * @param   array  $order
	 * @return  void
	 */
	function processPaymentSuccessRecurring($recurring_id, $payment_status, $order)
	{
		$order['amount'] = $this->getRegisterAmount();
		$order['currency'] = $this->session->data['currency'];

		if ($recurring_id) {
			$order['transaction_id'] = $payment_status['response']['merchantTransactionId'];
		} else {
			$order['transaction_id'] = (int)$this->customer->getId();
		}

		$order['payment_recurring'] = 'INITIAL';
		$order['test_mode'] = $this->getTestMode();

		$this->processPaymentSavedRecurring($recurring_id, $payment_status, $order);

		$this->session->data['success'] = 'register';

		if ($recurring_id) {
			$reference_id = $payment_status['response']['merchantTransactionId'];
			GenericshopPaymentCore::deleteRegisteredAccount($reference_id, $order);
			$this->session->data['success'] = 'change';
		}
		$this->response->redirect($this->url->link('account/payment_information', '', true));
	}

	/**
	 * Get payment response after change and register the payment acount
	 *
	 * @return  void
	 */
	public function callbackRecurring()
	{
		if ($this->isVersionTrackerActive()) {
			VersionTracker::sendVersionTracker($this->getVersionData());
		}
		$recurring_id = isset($this->request->get['recurring_id']) ? $this->request->get['recurring_id'] : '';
		$checkout_id = isset($this->request->get['id']) ? $this->request->get['id'] : '';
		$order = $this->getCredentials();
		$payment_status = GenericshopPaymentCore::getPaymentStatus($checkout_id, $order, $order['server_mode']);

		if (!$payment_status['is_valid']) {
			$this->redirectErrorRecurring($payment_status['response'], $recurring_id);
		} else {
			$result_code = $payment_status["response"]["result"]["code"];
			$error_identifer = GenericshopPaymentCore::getErrorIdentifier($result_code);
			$transaction_result = GenericshopPaymentCore::getTransactionResult($result_code);
			if ($transaction_result == "ACK") {
				$this->processPaymentSuccessRecurring($recurring_id, $payment_status, $order);
			} elseif ($transaction_result == "NOK") {
				$this->redirectErrorRecurring($error_identifer, $recurring_id);
			} else {
				$this->redirectErrorRecurring('ERROR_UNKNOWN', $recurring_id);
			}
		}
	}

	/**
	 * add an order history into order details
	 *
	 * @param   array  $payment_status
	 * @return  void
	 */
	function addOrderHistory($payment_status)
	{
		$this->language->load('extension/payment/genericshop');
		$this->load->model('checkout/order');

		if (GenericshopPaymentCore::isSuccessReview($payment_status['result']['code'])) {
			$order_status_id = $this->config->get('payment_genericshop_ageneral_review_status_id');
		} elseif ($payment_status['paymentType']=="PA") {
			$order_status_id = $this->config->get('payment_genericshop_ageneral_pa_status_id');
		} else {
			$order_status_id = $this->config->get('payment_genericshop_ageneral_accept_status_id');
		}

		$comment = $this->language->get('BACKEND_TT_TRANSACTION_ID') . ': ' . $payment_status['merchantTransactionId'] . '<br \>';

		$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $order_status_id, $comment);
	}

	/**
	 * redirect to the payment success page
	 *
	 * @param array $payment_status
	 * @return  void
	 */
	function redirectSuccess($payment_status)
	{
		$this->addGenericOrder($payment_status);
		$this->addOrderHistory($payment_status);
		$this->response->redirect($this->url->link('checkout/success', '', true));
	}

	/**
	 * Insert an order data into the the Generic Shop database
	 *
	 * @param array $payment_status
	 * @return  void
	 */
	function addGenericOrder($payment_status)
	{
		$this->load->model('genericshop/genericshop');

		if (GenericshopPaymentCore::isSuccessReview($payment_status['result']['code'])) {
			$payment_type = 'IR';
		} else {
			$payment_type = $payment_status['paymentType'];
		}

		$data['orders_id'] = $this->session->data['order_id'];
		$data['payment_method'] = $this->code;
		$data['unique_id'] =  $payment_status['id'];
		$data['amount'] = $payment_status['amount'];
		$data['currency'] = $payment_status['currency'];
		$data['payment_type'] = $payment_type;

		$this->model_genericshop_genericshop->saveOrder($data);
	}

	/**
	 * Process the payment success
	 *
	 * @param array $payment_status
	 * @return  void
	 */
	function processPaymentSuccess($payment_status)
	{
		if ($this->isRecurring()) {
			$registration_id = $payment_status['registrationId'];
			$this->insertRegistration($registration_id, $payment_status);
		}
		$this->redirectSuccess($payment_status);
	}

	/**
	 * Get a payment response then redirect to the payment success page or the payment error page.
	 *
	 * @return  void
	 */
	public function callback()
	{
		if ($this->isVersionTrackerActive()) {
			VersionTracker::sendVersionTracker($this->getVersionData());
		}
		$checkout_id = isset($this->request->get['id']) ? $this->request->get['id'] : '';
		$registration_id = isset($this->request->post['registrationId']) ? $this->request->post['registrationId'] : '';

		$order = $this->getCredentials();
		$payment_status = GenericshopPaymentCore::getPaymentStatus($checkout_id, $order, $order['server_mode']);

		if (!$payment_status["is_valid"]) {
			$this->redirectError($payment_status["response"]);
		} else {
			$result_code = $payment_status["response"]["result"]["code"];
			$error_identifer = GenericshopPaymentCore::getErrorIdentifier($result_code);
			$transaction_result = GenericshopPaymentCore::getTransactionResult($result_code);

			if ($transaction_result == "ACK") {
				$this->processPaymentSuccess($payment_status["response"]);
			} elseif ($transaction_result == 'NOK') {
				$this->redirectError($error_identifer);
			} else {
				$this->redirectError('ERROR_UNKNOWN');
			}
		}
	}

	public function getProductCartItems()
	{
		$data['products'] = array();
		$products = $this->cart->getProducts();

		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}

			if ($product['minimum'] > $product_total) {
				$data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
			}

			if ($product['image']) {
				$image = $this->model_tool_image->resize($product['image'], $this->config->get('theme_' . $this->config->get('config_theme') . '_image_cart_width'), $this->config->get('theme_' . $this->config->get('config_theme') . '_image_cart_height'));
			} else {
				$image = '';
			}

			$option_data = array();

			foreach ($product['option'] as $option) {
				if ($option['type'] != 'file') {
					$value = $option['value'];
				} else {
					$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

					if ($upload_info) {
						$value = $upload_info['name'];
					} else {
						$value = '';
					}
				}

				$option_data[] = array(
					'name'  => $option['name'],
					'value' => ((utf8_strlen($value) > 20) ? utf8_substr($value, 0, 20) . '..' : $value)
				);
			}

			// Display prices
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$price = false;
			}

			// Display prices
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'], $this->session->data['currency']);
			} else {
				$total = false;
			}

			$recurring = '';

			if ($product['recurring']) {
				$frequencies = array(
					'day'        => $this->language->get('text_day'),
					'week'       => $this->language->get('text_week'),
					'semi_month' => $this->language->get('text_semi_month'),
					'month'      => $this->language->get('text_month'),
					'year'       => $this->language->get('text_year'),
				);

				if ($product['recurring']['trial']) {
					$recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']) . ' ';
				}

				if ($product['recurring']['duration']) {
					$recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
				} else {
					$recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
				}
			}

			$data['products'][] = array(
				'cart_id'   => $product['cart_id'],
				'thumb'     => $image,
				'name'      => $product['name'],
				'model'     => $product['model'],
				'option'    => $option_data,
				'recurring' => $recurring,
				'quantity'  => $product['quantity'],
				'stock'     => (($product['stock']) ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))),
				'reward'    => (($product['reward']) ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
				'price'     => $price,
				'total'     => $total,
				'href'      => $this->url->link('product/product', 'product_id=' . $product['product_id'])
			);
		}

		return $data;
	}

	public function confirmOrder()
	{
		$checkout_id = isset($this->request->post['checkoutId']) ? $this->request->post['checkoutId'] : '';
		$transaction_data = $this->getCredentials();

		$transaction_data['test_mode'] = $this->getTestMode();
		$transaction_data['payment_type'] = "CP";
		$transaction_data['amount'] = isset($this->request->post['amount']) ? $this->request->post['amount'] : '';
		$transaction_data['currency'] = isset($this->request->post['currency']) ? $this->request->post['currency'] : '';
		$cart_total = '';

		$this->load->model('genericshop/genericshop');
		$carts = $this->model_genericshop_genericshop->getCartAmount();
		foreach ($carts as $cart) {
			if ($cart['code'] == 'total') {
				$cart_total = $this->currency->format($cart['value'], $this->session->data['currency']);
			}
		}

		$cart_total = str_ireplace('€', '', $cart_total);
		$cart_total = str_ireplace(',', '', $cart_total);
		if ($cart_total != $transaction_data['amount']) {
			$this->redirectError('ERROR_GENERAL_CAPTURE_PAYMENT');
		} else {
			$payment_status = GenericshopPaymentCore::backOfficeOperation($checkout_id, $transaction_data);

			if (!$payment_status["is_valid"]) {
				$this->redirectError($payment_status["response"]);
			} else {
				$result_code = $payment_status["response"]["result"]["code"];
				$transaction_result = GenericshopPaymentCore::getTransactionResult($result_code);

				if ($transaction_result == "ACK") {
					$this->processPaymentSuccess($payment_status["response"]);
				} elseif ($transaction_result == "NOK") {
					$error_identifier = GenericshopPaymentCore::getErrorIdentifier($result_code);
					$this->redirectError($error_identifier);
				} else {
					$this->redirectError($payment_status["response"]);
				}
			}
		}
	}
}
