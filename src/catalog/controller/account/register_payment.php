<?php
/* register payment controller
 * this controller to register payment account.
 *
 * @version 3.0.0
 * @date 2018-04-25
 *
 */
include_once(dirname(__FILE__) . '/../genericshop/genericshop.php');

class ControllerAccountRegisterPayment extends ControllerGenericshop {

	/**
	 * this function is the constructor of ControllerAccountRegisterPayment class
	 *
	 * @return  void
	 */
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/change_payment', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/account');
		$this->load->language('extension/payment/genericshop');
		$this->load->model('genericshop/genericshop');

		$this->document->setTitle($this->language->get('FRONTEND_MC_SAVE'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('FRONTEND_MC_INFO'),
			'href'      => $this->url->link('account/payment_information', '', true)
		);

		$data['FRONTEND_MC_SAVE'] = $this->language->get('FRONTEND_MC_SAVE');
		$data['FRONTEND_MC_CC'] = $this->language->get('FRONTEND_MC_CC');
		$data['FRONTEND_BT_CANCEL'] = $this->language->get('FRONTEND_BT_CANCEL');
		$data['FRONTEND_BT_REGISTER'] = $this->language->get('FRONTEND_BT_REGISTER');
		$data['FRONTEND_TT_TESTMODE'] = $this->language->get('FRONTEND_TT_TESTMODE');
		$data['FRONTEND_TT_REGISTRATION'] = $this->language->get('FRONTEND_TT_REGISTRATION');

		$selected_payment = $this->request->request['selected_payment'];

		$this->setPaymentMethodAtrributes($selected_payment);

		$order_data = $this->getOrderData();

		$checkout_result = $this->getCheckoutResultRecurring($order_data);

	   	if($checkout_result['is_valid']) {
			if (isset($this->session->data['error']))
			{
				$data['error'] = $this->language->get('ERROR_MC_ADD') . ' ' . $this->language->get($this->session->data['error']);
		    	unset($this->session->data['error']);
		    }

			$checkout_id = $checkout_result['response']['id'];

			if(empty($checkout_id)) {
				$this->session->data['error'] = $this->language->get('ERROR_GENERAL_REDIRECT');
				$this->response->redirect($this->url->link('account/payment_information', '', true));
			}

			$payment_widget_url = GenericshopPaymentCore::getPaymentWidgetUrl($order_data, $checkout_id);

			$data['lang'] = $this->getLangCode();
			$data['test_mode'] = $this->getTestMode();
			$data['brand'] = $this->getBrand();
			$data['redirect'] = $this->isRedirect();
			$data['payment_widget_url'] = $payment_widget_url;
			$data['cancel_url'] = $this->url->link('account/payment_information', '', true);
			$data['response_url'] = $this->url->link('extension/payment/' . $this->code . '/callbackRecurring', '', true);

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');
			$data['config_template'] = $this->config->get('config_template');

			$this->response->setOutput($this->load->view('account/register_payment', $data));
		}
		else {
			$this->session->data['error'] = $this->language->get($checkout_result['response']);
			$this->response->redirect($this->url->link('account/payment_information', '', true));
		}

	}
}
