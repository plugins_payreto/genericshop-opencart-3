<?php
/* payment information controller
 * this controller to show payment account page.
 *
 * @version 3.0.0
 * @date 2018-04-25
 *
 */
class ControllerAccountPaymentInformation extends Controller {

	/**
	 * this function is the constructor of ControllerAccountPaymentInformation class
	 *
	 * @return  void
	 */
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/payment_information', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/account');
		$this->load->language('extension/payment/genericshop');

		$this->document->setTitle($this->language->get('FRONTEND_MC_INFO'));

		$this->load->model('genericshop/genericshop');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('FRONTEND_MC_INFO'),
			'href'      => $this->url->link('account/payment_information', '', true)
		);

		if (isset($this->session->data['success'])) {
			switch ($this->session->data['success']) {
				case 'register':
					$data['success'] = $this->language->get('SUCCESS_MC_ADD');
					break;
				case 'change':
					$data['success'] = $this->language->get('SUCCESS_MC_UPDATE');
					break;
				case 'delete':
					$data['success'] = $this->language->get('SUCCESS_MC_DELETE');
					break;
			}
			unset($this->session->data['success']);
		}

		if (isset($this->session->data['error'])) {
			$data['error'] = $this->session->data['error'];
			unset($this->session->data['error']);
		}

		$data['is_recurring_active'] = $this->config->get('payment_genericshop_ageneral_recurring') == '1';
		$data['is_cc_recurring_active'] = $this->config->get('payment_genericshop_cc_saved_status') == '1';
		$data['is_dd_recurring_active'] = $this->config->get('payment_genericshop_dd_saved_status') == '1';

		if ($data['is_recurring_active']) {
			if ($data['is_cc_recurring_active']) {
				$data['customer_cc_account'] = $this->model_genericshop_genericshop->getPaymentAccount('genericshop_cc_saved');
			}
		}

		$data['FRONTEND_MC_INFO'] = $this->language->get('FRONTEND_MC_INFO');
		$data['FRONTEND_MC_CC'] = $this->language->get('FRONTEND_MC_CC');
		$data['FRONTEND_MC_ENDING'] = $this->language->get('FRONTEND_MC_ENDING');
		$data['FRONTEND_MC_VALIDITY'] = $this->language->get('FRONTEND_MC_VALIDITY');

		$data['FRONTEND_MC_DD'] = $this->language->get('FRONTEND_MC_DD');
		$data['FRONTEND_MC_ACCOUNT'] = $this->language->get('FRONTEND_MC_ACCOUNT');

		$data['FRONTEND_MC_EMAIL'] = $this->language->get('FRONTEND_MC_EMAIL');

		$data['FRONTEND_MC_BT_DEFAULT'] = $this->language->get('FRONTEND_MC_BT_DEFAULT');
		$data['FRONTEND_MC_BT_SETDEFAULT'] = $this->language->get('FRONTEND_MC_BT_SETDEFAULT');
		$data['FRONTEND_MC_BT_CHANGE'] = $this->language->get('FRONTEND_MC_BT_CHANGE');
		$data['FRONTEND_MC_BT_DELETE'] = $this->language->get('FRONTEND_MC_BT_DELETE');
		$data['FRONTEND_MC_BT_ADD'] = $this->language->get('FRONTEND_MC_BT_ADD');

		$data['back'] = $this->url->link('account/account', '', true);
		$data['default_payment'] = $this->url->link('account/payment_information/setDefault', '', true);
		$data['register_payment'] = $this->url->link('account/register_payment', '', true);
		$data['change_payment'] = $this->url->link('account/change_payment', '', true);
		$data['delete_payment'] = $this->url->link('account/delete_payment', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$data['config_template'] = $this->config->get('config_template');

		$this->response->setOutput($this->load->view('account/payment_information', $data));

	}

	/**
	 * Set default the payment acount.
	 *
	 * @return  void
	 */
	public function setDefault() {
		$this->load->model('genericshop/genericshop');

		$id = $this->request->post['id'];
		$payment_group = $this->request->post['payment_group'];

		if (isset($id) && isset($payment_group)) {
			$this->model_genericshop_genericshop->setDefault($id, $payment_group);
		}

		$this->response->redirect($this->url->link('account/payment_information', '', true));
	}
}
