<?php
/* delete payment controller
 * this controller to delete payment account.
 *
 * @version 3.0.0
 * @date 2018-04-25
 *
 */
include_once(dirname(__FILE__) . '/../genericshop/genericshop.php');

class ControllerAccountDeletePayment extends ControllerGenericshop {

	/**
	 * this function is the constructor of ControllerAccountDeletePayment class
	 *
	 * @return  void
	 */
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/delete_payment', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/account');
		$this->load->language('extension/payment/genericshop');
		$this->load->model('genericshop/genericshop');

		$this->document->setTitle($this->language->get('FRONTEND_MC_DELETE'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('FRONTEND_MC_INFO'),
			'href'      => $this->url->link('account/payment_information', '', true)
		);

		$data['FRONTEND_MC_DELETE'] = $this->language->get('FRONTEND_MC_DELETE');
		$data['FRONTEND_MC_DELETESURE'] = $this->language->get('FRONTEND_MC_DELETESURE');
		$data['FRONTEND_BT_CANCEL'] = $this->language->get('FRONTEND_BT_CANCEL');
		$data['FRONTEND_BT_CONFIRM'] = $this->language->get('FRONTEND_BT_CONFIRM');

		if(empty($this->request->post['selected_payment'])) {
			$this->session->data['error'] = $this->language->get('ERROR_MC_DELETE');
			$this->response->redirect($this->url->link('account/payment_information', '', true));
		}

		$selected_payment = $this->request->post['selected_payment'];
		$id = $this->request->post['id'];

		$this->code = $selected_payment;

		if (isset($this->request->post['action'])) {
			$response = $this->deletePaymentAccount($id);
			if ($response == 'ACK') {
				$this->session->data['success'] = 'delete';
				$this->response->redirect($this->url->link('account/payment_information', '', true));
			} else {
				$data['error'] = $this->language->get('ERROR_MC_DELETE');
			}
		}

		$data['id'] = $id;
		$data['selected_payment'] = $selected_payment;
		$data['cancel_url'] = $this->url->link('account/payment_information', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		$data['config_template'] = $this->config->get('config_template');

		$this->response->setOutput($this->load->view('account/delete_payment', $data));
	}
}
