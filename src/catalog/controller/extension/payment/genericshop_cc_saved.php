<?php
/* Generic Shop Credit Cards (recurring) payment controller
 *
 * @version 3.0.0
 * @date 2018-04-25
 *
 */
include_once(dirname(__FILE__) . '/../../genericshop/genericshop.php');

class ControllerExtensionPaymentGenericshopCcSaved extends ControllerGenericshop {
	protected $code = 'genericshop_cc_saved';
	protected $account_type = 'card';
	protected $recurring = true;
	protected $group_recurring = 'CC';

	/**
	 * this function is the constructor of ControllerExtensionPaymentGenericshopCcSaved class
	 *
	 * @return  void
	 */
	public function index() {

		return $this->confirmHtml();
	}

	/**
	 * Get a payment type
	 *
	 * @return  string
	 */
	function getPaymentType()
	{
	  return $this->getPaymentTypeSelection();
	}

	/**
	 * Get version data to be sent to version tracker
	 *
	 * @return  array
	 */
	function getVersionData()
	{
		$version_data = parent::getVersionData();
		$version_data['merchant_location'] = $this->getMerchantLocation();

		return $version_data;
	}
}
