<?php
/* Generic Shop Credit Cards payment controller
 *
 * @version 3.0.0
 * @date 2018-04-25
 *
 */
include_once(dirname(__FILE__) . '/../../genericshop/genericshop.php');

class ControllerExtensionPaymentGenericshopCc extends ControllerGenericshop {
	protected $code = 'genericshop_cc';
	protected $logo = "visa.png";

	/**
	 * this function is the constructor of ControllerExtensionPaymentGenericshopCc class
	 *
	 * @return  void
	 */
	public function index() {

		return $this->confirmHtml();
	}

	/**
	 * Get a payment type
	 *
	 * @return  string
	 */
	function getPaymentType()
	{
	  return $this->getPaymentTypeSelection();
	}

	/**
	 * Get version data to be sent to version tracker
	 *
	 * @return  array
	 */
	function getVersionData()
	{
		$version_data = parent::getVersionData();
		$version_data['merchant_location'] = $this->getMerchantLocation();

		return $version_data;
	}
}
