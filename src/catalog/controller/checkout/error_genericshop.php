<?php
/* Error checkout controller
 *
 * @version 3.0.0
 * @date 2018-04-25
 *
 */
class ControllerCheckoutErrorGenericshop extends Controller {

	/**
	 * this function is constructor of ControllerCheckoutErrorGenericshop class
	 *
	 * @return  void
	 */
	public function index() {

		$this->language->load('checkout/checkout');
		$this->language->load('extension/payment/genericshop');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_cart'),
			'href'      => $this->url->link('checkout/cart')
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('checkout/checkout', '', true)
		);

		$data['breadcrumbs'][] = array(
			'href'      => $this->url->link('checkout/error_genericshop&error_message=' . $this->request->get['error_message']),
			'text'      => $this->language->get('text_error')
		);

		$data['error'] = $this->language->get('text_error');

		$data['button_continue'] = $this->language->get('button_continue');

		$data['continue'] = $this->url->link('common/home');

		$data['error_title'] = $this->language->get('text_error_title');

		$data['error_message'] = $this->request->get['error_message'];

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('extension/payment/genericshop/error', $data));
	}
}
