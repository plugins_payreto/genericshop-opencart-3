<?php
/* Generic Shop Credit Cards (recurring) checkout controller
 *
 * @version 3.0.0
 * @date 2018-04-25
 *
 */
include_once(dirname(__FILE__) . '/../genericshop/genericshop.php');

class ControllerCheckoutGenericshopCcSaved extends ControllerGenericshop {
	protected $account_type = 'card';
	protected $group_recurring = 'CC';
	protected $code = "genericshop_cc_saved";
	protected $brand = "VISA MASTER AMEX DINERS JCB";
	protected $logo = "visa.png";
	protected $recurring = true;

	/**
	 * Get a payment type
	 *
	 * @return  string
	 */
	function getPaymentType()
	{
	  return $this->getPaymentTypeSelection();
	}

	/**
	 * Get the brands
	 *
	 * @return  string
	 */
	function getBrand() {
	  return $this->getBrandCards();
	}

	/**
	 * Get parameters on checkout
	 *
	 * @return  array
	 */
	public function getCheckoutParameters()
	{
		$checkout_parameters = parent::getCheckoutParameters();
		$checkout_parameters['3D']['amount'] = $checkout_parameters['amount'];
		$checkout_parameters['3D']['currency'] = $checkout_parameters['currency'];

		return $checkout_parameters;
	}
}
