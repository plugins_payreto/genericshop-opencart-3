<?php
/* Generic Shop Credit Cards checkout controller
 *
 * @version 3.0.0
 * @date 2018-04-25
 *
 */
include_once(dirname(__FILE__) . '/../genericshop/genericshop.php');

class ControllerCheckoutGenericshopCc extends ControllerGenericshop {
	protected $code = "genericshop_cc";
	protected $brand = "VISA MASTER AMEX DINERS JCB";
	protected $logo = "visa.png";

	/**
	 * Get a payment type
	 *
	 * @return  string
	 */
	function getPaymentType()
	{
	  return $this->getPaymentTypeSelection();
	}

	/**
	 * Get the brands
	 *
	 * @return  string
	 */
	function getBrand() {
	  return $this->getBrandCards();
	}
}
