<?php
// Text
$_['FRONTEND_PM_CC'] = 'Kreditkarte';
$_['FRONTEND_MC_PM_CARDSSAVED'] = 'Kreditkarte (Recurring)';

$_['FRONTEND_MC_PAYANDSAFE'] = 'Zahlung abschlie&szlig;en und Daten hinterlegen';
$_['FRONTEND_RECURRING_WIDGET_HEADER1'] = 'Hinterlegte Zahlungsdaten verwenden';
$_['FRONTEND_RECURRING_WIDGET_HEADER2'] = 'Alternative Zahlungsdaten verwenden';
$_['FRONTEND_TT_TESTMODE'] = 'TESTMODUS : ES FINDET KEINE REALE ZAHLUNG STATT';
$_['FRONTEND_BT_CANCEL'] = 'Abbrechen';
$_['FRONTEND_BT_PAYNOW'] = 'Jetzt bezahlen';

$_['FRONTEND_MC_INFO'] = 'Meine Zahlungsarten';
$_['FRONTEND_MC_CHANGE'] = 'Zahlungsarten &Auml;ndern';
$_['FRONTEND_MC_SAVE'] = 'Zahlungsarten anlegen';
$_['FRONTEND_MC_DELETE'] = 'Zahlungsarten l&ouml;schen';

$_['FRONTEND_MC_DELETESURE'] = 'Sind Sie sicher, dass Sie diese Zahlungsart l&ouml;schen m&ouml;chten?';
$_['FRONTEND_BT_CONFIRM'] = 'Best&auml;tigen';
$_['FRONTEND_MC_BACK'] = 'Zur&uuml;ck';

$_['FRONTEND_MC_CC'] = 'Kreditkarten';
$_['FRONTEND_MC_CCSELECT'] = 'Kreditkarte ausw&auml;hlen';
$_['FRONTEND_MC_CARDNEW'] = 'Eine neue Karte hinzuf&uuml;gen';
$_['FRONTEND_MC_VALIDITY'] = 'g&uuml;ltig bis:';
$_['FRONTEND_MC_ENDING'] = 'endet auf:';

$_['FRONTEND_MC_ACCOUNT'] = 'Konto: ****';
$_['FRONTEND_MC_HOLDER'] = 'Inhaber:';

$_['FRONTEND_MC_EMAIL'] = 'Email:';

$_['FRONTEND_MC_BT_CHANGE'] = '&Auml;ndern';
$_['FRONTEND_BT_REGISTER'] = 'Registrieren';
$_['FRONTEND_TT_REGISTRATION'] = 'Hinweis: Bei der Registrierung wird zur Verifizierung Ihrer Konten/Kartendaten ein kleiner Betrag <1 &euro; belastet und sofort wieder gut geschrieben.';
$_['FRONTEND_MC_BT_DEFAULT'] = 'Standard';
$_['FRONTEND_MC_BT_SETDEFAULT'] = 'Als Standard setzen';
$_['FRONTEND_MC_BT_DELETE'] = 'Entfernen';
$_['FRONTEND_MC_BT_ADD'] = 'Hinzuf&uuml;gen';
$_['FRONTEND_MC_BT_SELECT'] = 'Ausw&auml;hlen';

$_['SUCCESS_MC_ADD'] = 'Ihre Zahlungsart wurde erfolgreich angelegt';
$_['SUCCESS_MC_UPDATE'] = 'Ihre Zahlungsdaten wurden erfolgreich ge&auml;ndert';
$_['SUCCESS_MC_DELETE'] = 'Ihre Zahlungsart wurde erfolgreich gel&ouml;scht';

$_['ERROR_MC_ADD'] = 'Leider ist das Anlegen Ihrer Zahlungsart fehlgeschlagen.';
$_['ERROR_MC_UPDATE'] = 'Leider ist die &Auml;nderung Ihrer Zahlungsdaten fehlgeschlagen.';
$_['ERROR_MC_DELETE'] = 'Leider ist das l&ouml;schen Ihrer Zahlungsart fehlgeschlagen. Bitte versuchen Sie es erneut';
$_['ERROR_GENERAL_REDIRECT'] = 'Leider kam es zu einem Fehler bei der Weiterleitung';
$_['ERROR_GENERAL_NORESPONSE'] = 'Leider konnte ihre Zahlung nicht bestätigt werden. Bitte setzen Sie sich mit dem Händler in Verbindung.';
$_['ERROR_MERCHANT_SSL_CERTIFICATE'] = 'SSL-Zertifikat Problem, wenden Sie sich bitte an den Händler.';

$_['ERROR_CC_ACCOUNT'] = 'Sie sind nicht der Inhaber des eingegebenen Kontos. Bitte w&auml;hlen Sie ein Konto das auf Ihren Namen l&auml;uft.';
$_['ERROR_CC_INVALIDDATA'] = 'Ihre Karten-/Kontodaten sind leider nicht korrekt. Bitte versuchen Sie es erneut.';
$_['ERROR_CC_BLACKLIST'] = 'Leider kann die eingegebene Kreditkarte nicht akzeptiert werden. Bitte w&auml;hlen Sie eine andere Karte oder Bezahlungsmethode.';
$_['ERROR_CC_DECLINED_CARD'] = 'Leider kann die eingegebene Kreditkarte nicht akzeptiert werden. Bitte w&auml;hlen Sie eine andere Karte oder Bezahlungsmethode.';
$_['ERROR_CC_EXPIRED'] = 'Leider ist die eingegebene Kreditkarte abgelaufen. Bitte w&auml;hlen Sie eine andere Karte oder Bezahlungsmethode.';
$_['ERROR_CC_INVALIDCVV'] = 'Leider ist die eingegebene Kartenpr&uuml;fnummer nicht korrekt. Bitte versuchen Sie es erneut.';
$_['ERROR_CC_EXPIRY'] = 'Leider ist das eingegebene Ablaufdatum nicht korrekt. Bitte versuchen Sie es erneut.';
$_['ERROR_CC_LIMIT_EXCEED'] = 'Leider &uuml;bersteigt der zu zahlende Betrag das Limit Ihrer Kreditkarte. Bitte w&auml;hlen Sie eine andere Karte oder Bezahlsmethode.';
$_['ERROR_CC_3DAUTH'] = 'Ihr Passwort wurde leider nicht korrekt eingegeben. Bitte versuchen Sie es erneut.';
$_['ERROR_CC_3DERROR'] = 'Leider gab es einen Fehler bei der Durchf&uuml;hrung Ihrer Zahlung. Bitte versuchen Sie es erneut.';
$_['ERROR_CC_NOBRAND'] = 'Leider gab es einen Fehler bei der Durchf&uuml;hrung Ihrer Zahlung. Bitte versuchen Sie es erneut.';
$_['ERROR_GENERAL_LIMIT_AMOUNT'] = 'Leider &uuml;bersteigt der zu zahlende Betrag Ihr Limit. Bitte w&auml;hlen Sie eine andere Bezahlsmethode.';
$_['ERROR_GENERAL_LIMIT_TRANSACTIONS'] = 'Leider &uuml;bersteigt der zu zahlende Betrag Ihr Transaktionslimit. Bitte w&auml;hlen Sie eine andere Bezahlsmethode.';
$_['ERROR_CC_DECLINED_AUTH'] = 'Leider ist Ihre Zahlung fehlgeschlagen. Bitte versuchen Sie es erneut.';
$_['ERROR_GENERAL_DECLINED_RISK'] = 'Leider ist Ihre Zahlung fehlgeschlagen. Bitte versuchen Sie es erneut.';
$_['ERROR_CC_ADDRESS'] = 'Leider konnten wir Ihre Kartendaten nicht akzeptieren. Ihre Adresse stimmt nicht mit der Herkunft Ihrer Karte &uuml;berein.';
$_['ERROR_GENERAL_CANCEL'] = 'Der Vorgang wurde auf Ihren Wunsch abgebrochen. Bitte versuchen Sie es erneut.';
$_['ERROR_CC_RECURRING'] = 'F&uuml;r die gewählte Karte wurden wiederkehrende Zahlungen deaktiviert. Bitte w&auml;len Sie eine andere Bezahlmethode.';
$_['ERROR_CC_REPEATED'] = 'Leider ist Ihre Zahlung fehlgeschlagen, da Sie mehrfach fehlerhafte Angaben gemacht haben. Bitte w&auml;len Sie eine andere Bezahlmethode.';
$_['ERROR_GENERAL_ADDRESS'] = 'Leider ist Ihre Zahlung fehlgeschlagen. Bitte kontrollieren Sie Ihre pers&ouml;nlichen Angaben.';
$_['ERROR_GENERAL_BLACKLIST'] = 'Die gew&auml;hlte Bezahlmethode steht leider nicht zur Verfügung. Bitte w&auml;len Sie eine andere Bezahlmethode.';
$_['ERROR_GENERAL_GENERAL'] = 'Leider konnten wir Ihre Transaktion nicht durchf&uuml;hren. Bitte versuchen Sie es erneut.';
$_['ERROR_GENERAL_TIMEOUT'] = 'Leider konnten wir Ihre Transaktion nicht durchf&uuml;hren. Bitte versuchen Sie es erneut.';
$_['ERROR_UNKNOWN'] = 'Leider konnten wir Ihre Transaktion nicht durchf&uuml;hren. Bitte versuchen Sie es erneut.';
$_['ERROR_GENERAL_FRAUD_DETECTION'] = 'Leider kam es bei der Abwicklung Ihrer Bestellung zu einem Fehler. Eventuell geleistete Zahlungen werden erstattet.';

$_['ERROR_CAPTURE_BACKEND'] = 'Die Transaktion kann nicht gecaptured werden';
$_['ERROR_REORDER_BACKEND'] = 'Der Kunde hat seine Bank angewiesen, keine wiederkehrenden Zahlungen mehr zuzulassen.';
$_['ERROR_REFUND_BACKEND'] = 'Die Transaktion kann nicht refunded oder reversed werden';
$_['ERROR_RECEIPT_BACKEND'] = 'Das Receipt kann nicht prozessiert werden';
$_['ERROR_ADDRESS_PHONE'] = 'Leider ist Ihre Zahlung fehlgeschlagen. Bitte geben Sie eine korrekte Telefonnummer an.';

$_['SUCCESS_IN_REVIEW'] = 'Die Transaktion war erfolgreich, sollte aber manuell überprüft werden. Bitte aktualisieren Sie den Status der Transaktion manuell nach 24 Stunden';

$_['BACKEND_TT_TRANSACTION_ID'] = 'Transaktions-ID';

$_['SUCCESS_GENERAL_UPDATE_PAYMENT'] = 'Der Status der Transaktion wurde erfolgreich aktualisiert.';
$_['SUCCESS_GENERAL_CAPTURE_PAYMENT'] = 'Die Zahlung wurde erfolgreich ausgelöst.';
$_['SUCCESS_GENERAL_REFUND_PAYMENT'] = 'Die Zahlung wurde erfolgreich erstattet.';

$_['ERROR_GENERAL_UPDATE_PAYMENT'] = 'Unfortunately, your attempt to update the payment failed.';
$_['ERROR_GENERAL_CAPTURE_PAYMENT'] = 'Leider konnte die Zahlung nicht ausgel&ouml;st werden.';
$_['ERROR_GENERAL_REFUND_PAYMENT'] = 'Leider konnte die Zahlung nicht gutgeschrieben werden.';


$_['FRONTEND_GENERAL_CONFIRM_ORDER'] = 'Order Confirmation';
$_['GENERAL_TEXT_CONFIRM_ORDER'] = 'Zahlungspflichtig bestellen';
$_['FRONTEND_MERCHANT_LOCATION_DESC'] = 'Zahlungsempfänger: ';
