<?php
// Text
$_['FRONTEND_PM_CC'] = 'Credit Card';
$_['FRONTEND_MC_PM_CARDSSAVED'] = 'Credit Card (Recurring)';

$_['FRONTEND_MC_PAYANDSAFE'] = 'Pay and Save Payment Information';
$_['FRONTEND_RECURRING_WIDGET_HEADER1'] = 'Use stored payment data';
$_['FRONTEND_RECURRING_WIDGET_HEADER2'] = 'Use alternative payment data';
$_['FRONTEND_TT_TESTMODE'] = 'THIS IS A TEST. NO REAL MONEY WILL BE TRANSFERED';
$_['FRONTEND_BT_CANCEL'] = 'Cancel';
$_['FRONTEND_BT_PAYNOW'] = 'Pay now';

$_['FRONTEND_MC_INFO'] = 'My Payment Information';
$_['FRONTEND_MC_CHANGE'] = 'Change Payment Information';
$_['FRONTEND_MC_SAVE'] = 'Save Payment Information';
$_['FRONTEND_MC_DELETE'] = 'Delete Payment Information';

$_['FRONTEND_MC_DELETESURE'] = 'Are you sure to delete this payment information?';
$_['FRONTEND_BT_CONFIRM'] = 'Confirm';
$_['FRONTEND_MC_BACK'] = 'Back';

$_['FRONTEND_MC_CC'] = 'Credit Card';
$_['FRONTEND_MC_CCSELECT'] = 'Select Credit Card';
$_['FRONTEND_MC_CARDNEW'] = 'Add a new credit card';
$_['FRONTEND_MC_VALIDITY'] = 'expires on:';
$_['FRONTEND_MC_ENDING'] = 'ending in:';

$_['FRONTEND_MC_ACCOUNT'] = 'Account: ****';
$_['FRONTEND_MC_HOLDER'] = 'Holder:';

$_['FRONTEND_MC_EMAIL'] = 'Email:';

$_['FRONTEND_MC_BT_CHANGE'] = 'Change';
$_['FRONTEND_BT_REGISTER'] = 'Register';
$_['FRONTEND_TT_REGISTRATION'] = 'A small amount (<1 &euro;) will be charged and instantly refunded to verify your account/card details.';
$_['FRONTEND_MC_BT_DEFAULT'] = 'Default';
$_['FRONTEND_MC_BT_SETDEFAULT'] = 'Set as Default';
$_['FRONTEND_MC_BT_DELETE'] = 'Delete';
$_['FRONTEND_MC_BT_ADD'] = 'Add';
$_['FRONTEND_MC_BT_SELECT'] = 'Select';

$_['SUCCESS_MC_ADD'] = 'Congratulations, your payment information were successfully saved.';
$_['SUCCESS_MC_UPDATE'] = 'Congratulations, your payment information were successfully updated.';
$_['SUCCESS_MC_DELETE'] = 'Congratulations, your payment information were successfully deleted.';

$_['ERROR_MC_ADD'] = 'We are sorry. Your attempt to save your payment information was not successful.';
$_['ERROR_MC_UPDATE'] = 'We are sorry. Your attempt to update your payment information was not successful.';
$_['ERROR_MC_DELETE'] = 'We are sorry. Your attempt to delete your payment information was not successful, please try again.';
$_['ERROR_GENERAL_REDIRECT'] = 'Error before redirect';
$_['ERROR_GENERAL_NORESPONSE'] = 'Unfortunately, the confirmation of your payment failed. Please contact your merchant for clarification.';
$_['ERROR_MERCHANT_SSL_CERTIFICATE'] = 'SSL certificate problem, please contact the merchant.';

$_['ERROR_CC_ACCOUNT'] = 'The account holder entered does not match your name. Please use an account that is registered on your name.';
$_['ERROR_CC_INVALIDDATA'] = 'Unfortunately, the card/account data you entered was not correct. Please try again.';
$_['ERROR_CC_BLACKLIST'] = 'Unfortunately, the credit card you entered can not be accepted. Please choose a different card or payment method.';
$_['ERROR_CC_DECLINED_CARD'] = 'Unfortunately, the credit card you entered can not be accepted. Please choose a different card or payment method.';
$_['ERROR_CC_EXPIRED'] = 'Unfortunately, the credit card you entered is expired. Please choose a different card or payment method.';
$_['ERROR_CC_INVALIDCVV'] = 'Unfortunately, the CVV/CVC you entered is not correct. Please try again.';
$_['ERROR_CC_EXPIRY'] = 'Unfortunately, the expiration date you entered is not correct. Please try again.';
$_['ERROR_CC_LIMIT_EXCEED'] = 'Unfortunately, the limit of your credit card is exceeded. Please choose a different card or payment method.';
$_['ERROR_CC_3DAUTH'] = 'Unfortunately, the password you entered was not correct. Please try again.';
$_['ERROR_CC_3DERROR'] = 'Unfortunately, there has been an error while processing your request. Please try again.';
$_['ERROR_CC_NOBRAND'] = 'Unfortunately, there has been an error while processing your request. Please try again.';
$_['ERROR_GENERAL_LIMIT_AMOUNT'] = 'Unfortunately, your credit limit is exceeded. Please choose a different card or payment method.';
$_['ERROR_GENERAL_LIMIT_TRANSACTIONS'] = 'Unfortunately, your limit of transaction is exceeded. Please try again later. ';
$_['ERROR_CC_DECLINED_AUTH'] = 'Unfortunately, your transaction has failed. Please choose a different card or payment method.';
$_['ERROR_GENERAL_DECLINED_RISK'] = 'Unfortunately, your transaction has failed. Please choose a different card or payment method.';
$_['ERROR_CC_ADDRESS'] = 'We are sorry. We could no accept your card as its origin does not match your address.';
$_['ERROR_GENERAL_CANCEL'] = 'You cancelled the payment prior to its execution. Please try again.';
$_['ERROR_CC_RECURRING'] = 'Recurring transactions have been deactivated for this credit card. Please choose a different card or payment method.';
$_['ERROR_CC_REPEATED'] = 'Unfortunately, your transaction has been declined due to invalid data. Please choose a different card or payment method.';
$_['ERROR_GENERAL_ADDRESS'] = 'Unfortunately, your transaction has failed. Please check the personal data you entered.';
$_['ERROR_GENERAL_BLACKLIST'] = 'The chosen payment method is not available at the moment. Please choose a different card or payment method.';
$_['ERROR_GENERAL_GENERAL'] = 'Unfortunately, your transaction has failed. Please try again.';
$_['ERROR_GENERAL_TIMEOUT'] = 'Unfortunately, your transaction has failed. Please try again. ';
$_['ERROR_UNKNOWN'] = 'Unfortunately, your transaction has failed. Please try again.';
$_['ERROR_GENERAL_FRAUD_DETECTION'] = 'Unfortunately, there was an error while processing your order. In case a payment has been made, it will be automatically refunded.';

$_['ERROR_CAPTURE_BACKEND'] = 'Transaction can not be captured';
$_['ERROR_REORDER_BACKEND'] = 'Card holder has advised his bank to stop this recurring payment';
$_['ERROR_REFUND_BACKEND'] = 'Transaction can not be refunded or reversed.';
$_['ERROR_RECEIPT_BACKEND'] = 'Receipt can not be performed';
$_['ERROR_ADDRESS_PHONE'] = 'Unfortunately, your transaction has failed. Please enter a valid telephone number.';

$_['SUCCESS_IN_REVIEW'] = 'Transaction succeeded but has been suspended for manual review. Please update transaction status in 24 hours.';

$_['BACKEND_TT_TRANSACTION_ID'] = 'Transaction ID';

$_['SUCCESS_GENERAL_UPDATE_PAYMENT'] = 'The payment status has been successfully updated.';
$_['SUCCESS_GENERAL_CAPTURE_PAYMENT'] = 'The payment has been successfully captured.';
$_['SUCCESS_GENERAL_REFUND_PAYMENT'] = 'The payment has been successfully refunded.';

$_['ERROR_GENERAL_UPDATE_PAYMENT'] = 'Unfortunately, your attempt to update the payment failed.';
$_['ERROR_GENERAL_CAPTURE_PAYMENT'] = 'Unfortunately, your attempt to capture the payment failed.';
$_['ERROR_GENERAL_REFUND_PAYMENT'] = 'Unfortunately, your attempt to refund the payment failed.';

$_['FRONTEND_GENERAL_CONFIRM_ORDER'] = 'Order Confirmation';
$_['GENERAL_TEXT_CONFIRM_ORDER'] = 'I confirm my order';
$_['FRONTEND_MERCHANT_LOCATION_DESC'] = 'Payee: ';
