<?php
/* Generic Shop controller in admin
 *
 * @version 3.0.0
 * @date 2018-04-25
 *
 */

class ControllerGenericshopAdmin extends Controller {
	protected $error = array();
	protected $keys = array();
	protected $payment_method_code = '';
	protected $configuration_code = '';

	/**
	 * this function is the constructor of ControllerGenericshopAdmin class
	 *
	 * @return  void
	 */
	public function index() {
		$this->load->language('extension/payment/' . $this->configuration_code);

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting($this->payment_method_code, $this->request->post);

			$this->session->data['success'] = $this->language->get('BACKEND_CH_SUCCESS');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['BACKEND_CH_EDIT'] = $this->language->get('BACKEND_CH_EDIT');

		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');

		$data['BACKEND_CH_SERVER'] = $this->language->get('BACKEND_CH_SERVER');
		$data['BACKEND_CH_MODE_LIVE'] = $this->language->get('BACKEND_CH_MODE_LIVE');
		$data['BACKEND_CH_MODE_TEST'] = $this->language->get('BACKEND_CH_MODE_TEST');
		$data['BACKEND_CH_MODE'] = $this->language->get('BACKEND_CH_MODE');
		$data['BACKEND_CH_MODEDEBIT'] = $this->language->get('BACKEND_CH_MODEDEBIT');
		$data['BACKEND_CH_MODEPREAUTH'] = $this->language->get('BACKEND_CH_MODEPREAUTH');
		$data['BACKEND_CH_PAYMENT_IS_PARTIAL'] = $this->language->get('BACKEND_CH_PAYMENT_IS_PARTIAL');
		$data['BACKEND_CH_MINIMUM_AGE'] = $this->language->get('BACKEND_CH_MINIMUM_AGE');

		$data['BACKEND_CH_CARDS'] = $this->language->get('BACKEND_CH_CARDS');
		$data['BACKEND_CH_AMOUNT'] = $this->language->get('BACKEND_CH_AMOUNT');
		$data['BACKEND_TT_REGISTRATION_AMOUNT'] = $this->language->get('BACKEND_TT_REGISTRATION_AMOUNT');
		$data['BACKEND_TT_MULTICHANNEL'] = $this->language->get('BACKEND_TT_MULTICHANNEL');
		$data['BACKEND_TT_CHANNEL_MOTO'] = $this->language->get('BACKEND_TT_CHANNEL_MOTO');
		$data['BACKEND_CH_MULTICHANNEL'] = $this->language->get('BACKEND_CH_MULTICHANNEL');
		$data['BACKEND_CH_CHANNEL'] = $this->language->get('BACKEND_CH_CHANNEL');
		$data['BACKEND_CH_MOTO'] = $this->language->get('BACKEND_CH_MOTO');

		$data['BACKEND_CH_TOTAL'] = $this->language->get('BACKEND_CH_TOTAL');
		$data['BACKEND_TT_TOTAL'] = $this->language->get('BACKEND_TT_TOTAL');
		$data['BACKEND_CH_ZONE'] = $this->language->get('BACKEND_CH_ZONE');
		$data['BACKEND_CH_STATUS'] = $this->language->get('BACKEND_CH_STATUS');
		$data['BACKEND_CH_ORDER'] = $this->language->get('BACKEND_CH_ORDER');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], true),
			'separator' => false
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('BACKEND_CH_PAYMENT'),
			'href'      => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true),
			'separator' => ' :: '
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/payment/' . $this->configuration_code, 'user_token=' . $this->session->data['user_token'], true),
			'separator' => ' :: '
		);

		$data['action'] = $this->url->link('extension/payment/' . $this->configuration_code, 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true);

		foreach ($this->keys as $value) {
			$key = $this->payment_method_code . '_' . $value;
			if (isset($this->request->post[$key])) {
				$data[$key] = $this->request->post[$key];
			} else {
				$data[$key] = $this->config->get($key);
			}
		}

		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/payment/' . $this->configuration_code, $data));
	}

	/**
	 * to validate another fields in the payment method configuration
	 *
	 * @return  boolean
	 */
	public function validate() {
		if (!$this->user->hasPermission('modify', 'extension/payment/' . $this->configuration_code)) {
			$this->error['warning'] = $this->language->get('ERROR_PERMISSION');
		}
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
