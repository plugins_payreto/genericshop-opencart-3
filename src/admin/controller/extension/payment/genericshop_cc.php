<?php
/* Generic Shop Credit Cards controller in admin
 *
 * @version 3.0.0
 * @date 2018-04-25
 *
 */
include_once(dirname(__FILE__) . '/../../genericshop/genericshop_admin.php');

class ControllerExtensionPaymentGenericshopCc extends ControllerGenericshopAdmin {
	protected $payment_method_code = 'payment_genericshop_cc';
	protected $configuration_code = 'genericshop_cc';
	protected $keys = array('server','transaction_mode','cards_types','channel_id','total','geo_zone_id','status','sort_order');
}
