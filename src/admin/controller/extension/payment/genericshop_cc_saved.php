<?php
/* Generic Shop Credit Cards (Recurring) controller in admin
 *
 * @version 3.0.0
 * @date 2018-04-25
 *
 */
include_once(dirname(__FILE__) . '/../../genericshop/genericshop_admin.php');

class ControllerExtensionPaymentGenericshopCcSaved extends ControllerGenericshopAdmin {
	protected $payment_method_code = 'payment_genericshop_cc_saved';
	protected $configuration_code = 'genericshop_cc_saved';
	protected $keys = array('server','transaction_mode','cards_types','amount','multichannel','channel_id','moto','total','geo_zone_id','status','sort_order');
}
