<?php
/* Generic Shop general controller in admin
 * Extra of the letter 'A' before the word 'general' in order to be in first place in the list of the payment module.
 *
 * @version 3.0.0
 * @date 2018-04-25
 *
 */
class ControllerExtensionPaymentGenericshopAgeneral extends Controller {
	private $error = array();
	private $keys = array(
		'login',
		'password',
		'recurring',
		'gender_customfield',
		'gender_malevalue',
		'gender_femalevalue',
		'dob_customfield',
		'merchant_no',
		'merchant_email',
		'shop_url',
		'version_tracker',
		'status',
		'merchant_location'
	);
	private $payment_method_code = 'payment_genericshop_ageneral';
	private $configuration_code = 'genericshop_ageneral';


	/**
	 * this function is the constructor of ControllerExtensionPaymentGenericshopAgeneral class
	 *
	 * @return  void
	 */
	public function index() {
		$this->load->language('extension/payment/' . $this->configuration_code);

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');
		$this->load->model('extension/payment/genericshop');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			$lang_id = $this->model_extension_payment_genericshop->getLangId();

			$this->load->model('localisation/order_status');

			$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
			foreach ($data['order_statuses'] as $key => $value) {
				if ($this->config->get('config_language_id') == $lang_id['en']) {
					if ($value['name'] == "Payment Accepted") {
						$order_statuses[$this->payment_method_code . '_accept_status_id'] = $value['order_status_id'];
					}
					if ($value['name'] == "Pre-Authorization of Payment") {
						$order_statuses[$this->payment_method_code . '_pa_status_id'] = $value['order_status_id'];
					}
					if ($value['name'] == "In Review") {
						$order_statuses[$this->payment_method_code . '_review_status_id'] = $value['order_status_id'];
					}
					if ($value['name'] == "Refunded") {
						$order_statuses[$this->payment_method_code . '_refund_status_id'] = $value['order_status_id'];
					}
				}
				if ($this->config->get('config_language_id') == $lang_id['de']) {
					if ($value['name'] == "Zahlung akzeptiert") {
						$order_statuses[$this->payment_method_code . '_accept_status_id'] = $value['order_status_id'];
					}
					if ($value['name'] == "Pre-Authorisierung der Zahlung") {
						$order_statuses[$this->payment_method_code . '_pa_status_id'] = $value['order_status_id'];
					}
					if ($value['name'] == "Wird überprüft") {
						$order_statuses[$this->payment_method_code . '_review_status_id'] = $value['order_status_id'];
					}
					if ($value['name'] == "Gutschrift") {
						$order_statuses[$this->payment_method_code . '_refund_status_id'] = $value['order_status_id'];
					}
				}
			}

			$config_post = array_merge($this->request->post, $order_statuses);
			$this->model_setting_setting->editSetting($this->payment_method_code, $config_post);

			$this->session->data['success'] = $this->language->get('BACKEND_CH_SUCCESS');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['BACKEND_CH_EDIT'] = $this->language->get('BACKEND_CH_EDIT');

		$data['BACKEND_CH_LOGIN'] = $this->language->get('BACKEND_CH_LOGIN');
		$data['BACKEND_CH_PASSWORD'] = $this->language->get('BACKEND_CH_PASSWORD');
		$data['BACKEND_CH_RECURRING'] = $this->language->get('BACKEND_CH_RECURRING');
		$data['BACKEND_CH_VERSION_TRACKER'] = $this->language->get('BACKEND_CH_VERSION_TRACKER');
		$data['BACKEND_GENERAL_GENDER_CUSTOMFIELD'] = $this->language->get('BACKEND_GENERAL_GENDER_CUSTOMFIELD');
		$data['BACKEND_GENERAL_GENDER_MALEVALUE'] = $this->language->get('BACKEND_GENERAL_GENDER_MALEVALUE');
		$data['BACKEND_GENERAL_GENDER_FEMALEVALUE'] = $this->language->get('BACKEND_GENERAL_GENDER_FEMALEVALUE');
		$data['BACKEND_GENERAL_DOB_CUSTOMFIELD'] = $this->language->get('BACKEND_GENERAL_DOB_CUSTOMFIELD');
		$data['BACKEND_GENERAL_MERCHANTEMAIL'] = $this->language->get('BACKEND_GENERAL_MERCHANTEMAIL');
		$data['BACKEND_GENERAL_MERCHANTNO'] = $this->language->get('BACKEND_GENERAL_MERCHANTNO');
		$data['BACKEND_GENERAL_SHOPURL'] = $this->language->get('BACKEND_GENERAL_SHOPURL');
		$data['BACKEND_TT_MERCHANT_ID'] = $this->language->get('BACKEND_TT_MERCHANT_ID');
		$data['BACKEND_TT_VERSION_TRACKER'] = $this->language->get('BACKEND_TT_VERSION_TRACKER');
		$data['BACKEND_CH_STATUS'] = $this->language->get('BACKEND_CH_STATUS');
		$data['BACKEND_GENERAL_MERCHANT_LOCATION_TITLE'] = $this->language->get('BACKEND_GENERAL_MERCHANT_LOCATION_TITLE');
		$data['BACKEND_GENERAL_MERCHANT_LOCATION_DESC'] = $this->language->get('BACKEND_GENERAL_MERCHANT_LOCATION_DESC');

		$data['custom_fields'] = $this->model_extension_payment_genericshop->getCustomFields();
		$data['custom_field_values'] = $this->model_extension_payment_genericshop->getCustomFieldValues();

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], true),
			'separator' => false
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('BACKEND_CH_PAYMENT'),
			'href'      => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true),
			'separator' => ' :: '
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/payment/' . $this->configuration_code, 'user_token=' . $this->session->data['user_token'], true),
			'separator' => ' :: '
		);

		$data['action'] = $this->url->link('extension/payment/' . $this->configuration_code, 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true);

		foreach ($this->keys as $value) {
			$key = $this->payment_method_code . '_' . $value;
			if (isset($this->request->post[$key])) {
				$data[$key] = $this->request->post[$key];
			} else {
				$data[$key] = $this->config->get($key);
			}
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/payment/' . $this->configuration_code, $data));
	}

	/**
	 * to install the payment method
	 *
	 * @return  void
	 */
	public function install() {
		$this->load->language('extension/payment/' . $this->configuration_code);

		$this->load->model('setting/setting');
		$this->load->model('extension/payment/genericshop');
		$this->model_extension_payment_genericshop->addCustomOrderStatuses();
		$this->model_extension_payment_genericshop->install();
		$this->model_setting_setting->editSetting(
			$this->payment_method_code,
			array(
				'payment_genericshop_ageneral_version_tracker' => 1,
				'payment_genericshop_ageneral_status' => 1
			)
		);
	}

	/**
	 * to validate another field in the payment method configuration
	 *
	 * @return  boolean
	 */
	public function validate() {
		if (!$this->user->hasPermission('modify', 'extension/payment/genericshop_ageneral')) {
			$this->error['warning'] = $this->language->get('ERROR_PERMISSION');
			return false;
		}
		if (empty($this->request->post['payment_genericshop_ageneral_merchant_email'])) {
			$this->error['warning'] = $this->language->get('BACKEND_GENERAL_MERCHANTEMAIL') . ' ' . $this->language->get('ERROR_MANDATORY');
			return false;
		}
		if (empty($this->request->post['payment_genericshop_ageneral_merchant_no'])) {
			$this->error['warning'] = $this->language->get('BACKEND_GENERAL_MERCHANTNO') . ' ' . $this->language->get('ERROR_MANDATORY');
			return false;
		}
		if (empty($this->request->post['payment_genericshop_ageneral_shop_url'])) {
			$this->error['warning'] = $this->language->get('BACKEND_GENERAL_SHOPURL') . ' ' . $this->language->get('ERROR_MANDATORY');
			return false;
		}
		if(empty($this->request->post['payment_genericshop_ageneral_merchant_location'])) {
			$this->error['warning'] = $this->language->get('BACKEND_GENERAL_MERCHANT_LOCATION_TITLE') . ' ' . $this->language->get('ERROR_MANDATORY');
			return false;
		}
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
