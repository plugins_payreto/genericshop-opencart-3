<?php
/* Generic Shop model in admin
 * to select, insert, update into the plugin database and create the plugin database
 *
 * @version 3.0.0
 * @date 2018-04-25
 *
 */
class ModelExtensionPaymentGenericshop extends Model {
	/**
	 * Get language ID
	 *
	 * @return array
	 */
	public function getLangId() {
		$this->load->model('localisation/language');

		$langs = $this->model_localisation_language->getLanguages();

		$lang_id['en'] = 0;
		$lang_id['de'] = 0;
		foreach ($langs as $lang) {
			if (substr($lang['code'], 0, 2) == 'en') {
				$lang_id['en'] = $lang['language_id'];
			}
			if (substr($lang['code'], 0, 2) == 'de') {
				$lang_id['de'] = $lang['language_id'];
			}
		}
		return $lang_id;
	}

	/**
	 * Get custom fields
	 *
	 * @return array
	 */
	public function getCustomFields() {
		$language_id = (int)$this->config->get('config_language_id');
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "custom_field_description WHERE language_id = '" . $language_id . "'");

		return $query->rows;
	}

	/**
	 * Get custom fields values
	 *
	 * @return array
	 */
	public function getCustomFieldValues() {
		$language_id = (int)$this->config->get('config_language_id');
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "custom_field_value_description WHERE language_id = '" . $language_id . "'");

		return $query->rows;
	}

	/**
	 * Adding Generic Shop order statuses into the opencart database
	 *
	 * @return void
	 */
	public function addCustomOrderStatuses() {
		$lang_id = $this->getLangId();
		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		$is_add_accepted = true;
		$is_add_pa = true;
		$is_add_in_review  = true;
		$is_add_refund = true;

		foreach ($data['order_statuses'] as $key => $value) {
			if ($this->config->get('config_language_id') == $lang_id['en'] && $value['name'] == "Payment Accepted") {
				$is_add_accepted = false;
			}
			if ($this->config->get('config_language_id') == $lang_id['de'] && $value['name'] == "Zahlung akzeptiert") {
				$is_add_accepted = false;
			}
			if ($this->config->get('config_language_id') == $lang_id['en'] && $value['name'] == "Pre-Authorization of Payment") {
				$is_add_pa = false;
			}
			if ($this->config->get('config_language_id') == $lang_id['de'] && $value['name'] == "Pre-Authorisierung der Zahlung") {
				$is_add_pa = false;
			}
			if ($this->config->get('config_language_id') == $lang_id['en'] && $value['name'] == "In Review") {
				$is_add_in_review = false;
			}
			if ($this->config->get('config_language_id') == $lang_id['de'] && $value['name'] == "Wird überprüft") {
				$is_add_in_review = false;
			}
			if ($this->config->get('config_language_id') == $lang_id['en'] && $value['name'] == "Refunded") {
				$is_add_refund = false;
			}
			if ($this->config->get('config_language_id') == $lang_id['de'] && $value['name'] == "Gutschrift") {
				$is_add_refund = false;
			}
		}

		if ($this->config->get('config_language_id') == $lang_id['en'] || $this->config->get('config_language_id') == $lang_id['de']) {
			if ($is_add_accepted) {
				if ($lang_id['en'] > 0) {
					$data['order_status'][$lang_id['en']]['name'] = "Payment Accepted";
				}
				if ($lang_id['de'] > 0) {
					$data['order_status'][$lang_id['de']]['name'] = "Zahlung akzeptiert";
				}
				$this->model_localisation_order_status->addOrderStatus($data);
			}

			if ($is_add_pa) {
				if ($lang_id['en'] > 0) {
					$data['order_status'][$lang_id['en']]['name'] = "Pre-Authorization of Payment";
				}
				if ($lang_id['de'] > 0) {
					$data['order_status'][$lang_id['de']]['name'] = "Pre-Authorisierung der Zahlung";
				}
				$this->model_localisation_order_status->addOrderStatus($data);
			}

			if ($is_add_in_review) {
				if ($lang_id['en'] > 0) {
					$data['order_status'][$lang_id['en']]['name'] = "In Review";
				}
				if ($lang_id['de'] > 0) {
					$data['order_status'][$lang_id['de']]['name'] = "Wird überprüft";
				}
				$this->model_localisation_order_status->addOrderStatus($data);
			}

			if ($is_add_refund) {
				if ($lang_id['en'] > 0) {
					$data['order_status'][$lang_id['en']]['name'] = "Refunded";
				}
				if ($lang_id['de'] > 0) {
					$data['order_status'][$lang_id['de']]['name'] = "Gutschrift";
				}
				$this->model_localisation_order_status->addOrderStatus($data);
			}
		}
	}

	/**
	 * Install the Generic Shop module
	 *
	 * @return void
	 */
	public function install() {
		$this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "payment_genericshop_orders` (
					`id` INT(11) NOT NULL AUTO_INCREMENT,
					`orders_id` INT(11) NOT NULL,
					`unique_id` VARCHAR(32) NOT NULL,
					`amount` DECIMAL(15,4) NOT NULL,
					`currency` VARCHAR(3) NOT NULL,
					`payment_type` VARCHAR(2) NOT NULL,
					`payment_method` VARCHAR(50) NOT NULL,
					PRIMARY KEY (`id`)
			) ENGINE=MyISAM; ");

		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "payment_genericshop_recurring` (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`cust_id` INT(11) NOT NULL,
					`payment_group` VARCHAR(32),
					`brand` VARCHAR(100),
					`holder` VARCHAR(100) NULL default NULL,
					`email` VARCHAR(100) NULL default NULL,
					`last4digits` VARCHAR(4),
					`expiry_month` VARCHAR(2),
					`expiry_year` VARCHAR(4),
					`ref_id` VARCHAR(32),
					`payment_default` boolean NOT NULL default '0',
					PRIMARY KEY (`id`)
			) ENGINE=MyISAM; ");

		$query = $this->db->query("SHOW columns FROM `" . DB_PREFIX . "payment_genericshop_recurring` LIKE 'server_mode'");
		if ($query->num_rows < 1) {
			$this->db->query("ALTER TABLE `" . DB_PREFIX . "payment_genericshop_recurring`
				ADD `server_mode` VARCHAR(4) NOT NULL AFTER `expiry_year`, ADD `channel_id` VARCHAR(32) NOT NULL AFTER `server_mode`");
		}
	}
}
