<?php
// Heading
$_['heading_title']		      		= 'Generic Shop General Settings';
$_['text_genericshop_ageneral']	= '<a href="http://www.generic-shop.info" target="_blank"><img src="view/image/payment/genericshop.png" alt="' . $_['heading_title'] . '" title="' . $_['heading_title'] . '" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_success']	= 'WARNING For providing the best service to you, to inform you about newer versions of the plugin and also about security issues, Generic Shop is gathering some basic and technical information from the shop system (for details please see the manual). The information will under no circumstances be used for marketing and/or advertising purposes. Please be aware that deactivating the version tracker may affect the service quality and also important security and update information.';

// Text
$_['BACKEND_CH_PAYMENT']			= 'Payment';
$_['BACKEND_CH_SUCCESS']			= 'Success : You have modified ' . $_['heading_title'] . ' details.';
$_['BACKEND_CH_EDIT']				= 'Edit ' . $_['heading_title'];

// Entry
$_['BACKEND_CH_LOGIN']  						= 'Login';
$_['BACKEND_CH_PASSWORD']  						= 'Password';
$_['BACKEND_CH_RECURRING']  					= 'Recurring';
$_['BACKEND_GENERAL_GENDER_CUSTOMFIELD']		= 'Gender custom field';
$_['BACKEND_GENERAL_GENDER_MALEVALUE']			= 'Gender male value';
$_['BACKEND_GENERAL_GENDER_FEMALEVALUE']		= 'Gender female value';
$_['BACKEND_GENERAL_DOB_CUSTOMFIELD']  			= 'Date of Birth custom field';
$_['BACKEND_GENERAL_MERCHANTEMAIL'] 			= 'Merchant E-mail Address';
$_['BACKEND_GENERAL_MERCHANTNO']    			= 'Merchant No. (Generic Shop)';
$_['BACKEND_GENERAL_SHOPURL']					= 'Shop URL';
$_['BACKEND_CH_STATUS']							= 'Status';
$_['BACKEND_CH_VERSION_TRACKER']				= 'Version Tracker';
$_['BACKEND_GENERAL_MERCHANT_LOCATION_TITLE']	= 'Merchant Location';
$_['BACKEND_GENERAL_MERCHANT_LOCATION_DESC']	= 'Principal place of business (Company Name, Adress including the Country)';

// Help
$_['BACKEND_TT_MERCHANT_ID']        = 'Your Customer ID from Generic Shop';
$_['BACKEND_TT_VERSION_TRACKER']    = 'When enabled, you accept to share your IP, email address, etc with Genericshop.';

// Error
$_['ERROR_PERMISSION']	      		= 'Warning : You do not have permission to modify ' . $_['heading_title'];
$_['ERROR_MANDATORY'] 				= 'is required. please fill out this field';
