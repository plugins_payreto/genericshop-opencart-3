<?php
// Heading
$_['heading_title']				= 'Generic Shop Credit Card';
$_['text_genericshop_cc']	= '<a href="http://www.generic-shop.info" target="_blank"><img src="view/image/payment/genericshop.png" alt="' . $_['heading_title'] . '" title="' . $_['heading_title'] . '" style="border: 1px solid #EEEEEE;" /></a>';

// Text
$_['BACKEND_CH_PAYMENT']		= 'Payment';
$_['BACKEND_CH_SUCCESS']		= 'Success : You have modified ' . $_['heading_title'] . ' details.';
$_['BACKEND_CH_EDIT']			= 'Edit ' . $_['heading_title'];

// Entry
$_['BACKEND_CH_SERVER']		    = 'Server';
$_['BACKEND_CH_MODE_LIVE']		= 'LIVE';
$_['BACKEND_CH_MODE_TEST']		= 'TEST';
$_['BACKEND_CH_MODE']  			= 'Transaction Mode';
$_['BACKEND_CH_MODEDEBIT']		= 'Debit';
$_['BACKEND_CH_MODEPREAUTH']	= 'Pre-Authorization';
$_['BACKEND_CH_CARDS']			= 'Cards Types';
$_['BACKEND_CH_CHANNEL']  	  	= 'Entity-ID';
$_['BACKEND_CH_TOTAL']          = 'Total';
$_['BACKEND_CH_ZONE']			= 'Geo Zone';
$_['BACKEND_CH_STATUS']			= 'Status';
$_['BACKEND_CH_ORDER']        	= 'Sort Order';

// Help
$_['BACKEND_TT_TOTAL']         	= 'The checkout total the order must reach before this payment method becomes active.';

// Error
$_['ERROR_PERMISSION']	      	= 'Warning : You do not have permission to modify ' . $_['heading_title'];
