<?php
// Heading
$_['heading_title']		      		= 'Generic Shop Allgemeine Einstellungen';
$_['text_genericshop_ageneral']	= '<a href="http://www.generic-shop.info" target="_blank"><img src="view/image/payment/genericshop.png" alt="' . $_['heading_title'] . '" title="' . $_['heading_title'] . '" style="border: 1px solid #EEEEEE;" /></a>';

// Text
$_['BACKEND_CH_PAYMENT']			= 'Zahlung';
$_['BACKEND_CH_SUCCESS']			= 'Erfolgreich : ' . $_['heading_title'] . ' ge&auml;ndert.';
$_['BACKEND_CH_EDIT']				= 'Bearbeiten ' . $_['heading_title'];
$_['text_success']	= 'WARNING For providing the best service to you, to inform you about newer versions of the plugin and also about security issues, Generic Shop is gathering some basic and technical information from the shop system (for details please see the manual). The information will under no circumstances be used for marketing and/or advertising purposes. Please be aware that deactivating the version tracker may affect the service quality and also important security and update information.';

// Entry
$_['BACKEND_CH_LOGIN']  						= 'Benutzer-Login';
$_['BACKEND_CH_PASSWORD']  						= 'Passwort';
$_['BACKEND_CH_RECURRING']  					= 'Recurring';
$_['BACKEND_GENERAL_GENDER_CUSTOMFIELD']		= 'Geschlecht benutzerdefiniertes Feld';
$_['BACKEND_GENERAL_GENDER_MALEVALUE']			= 'Geschlecht männlicher Wert';
$_['BACKEND_GENERAL_GENDER_FEMALEVALUE']		= 'Geschlecht weiblicher Wert';
$_['BACKEND_GENERAL_DOB_CUSTOMFIELD']  			= 'Geburtsdatum benutzerdefiniertes Feld';
$_['BACKEND_GENERAL_MERCHANTEMAIL'] 			= 'Händler E-Mail-Adressee';
$_['BACKEND_GENERAL_MERCHANTNO']    			= 'Händlernummer (Generic Shop)';
$_['BACKEND_GENERAL_SHOPURL']					= 'Shop URL';
$_['BACKEND_CH_STATUS']							= 'Status';
$_['BACKEND_CH_VERSION_TRACKER']				= 'Version Tracker';
$_['BACKEND_GENERAL_MERCHANT_LOCATION_TITLE']	= 'Firmensitz';
$_['BACKEND_GENERAL_MERCHANT_LOCATION_DESC']	= 'Firmensitz lt. Handelsregister (Firmenname, Adresse inklusive Land)';

// Help
$_['BACKEND_TT_MERCHANT_ID']        = 'Ihre Kundennummer/Händlernummer bei Generic Shop';
$_['BACKEND_TT_VERSION_TRACKER']    = 'When enabled, you accept to share your IP, email address, etc with Genericshop.';

// Error
$_['ERROR_PERMISSION']	      		= 'Warnung : Sie haben keine Berechtigung, um ' . $_['heading_title'] . ' zu &auml;ndern!';
$_['ERROR_MANDATORY'] 				= 'is required. please fill out this field';
