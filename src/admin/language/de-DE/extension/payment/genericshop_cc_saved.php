<?php
// Heading
$_['heading_title']						= 'Generic Shop Kreditkarte (Recurring)';
$_['text_genericshop_cc_saved']		= '<a href="http://www.generic-shop.info" target="_blank"><img src="view/image/payment/genericshop.png" alt="' . $_['heading_title'] . '" title="' . $_['heading_title'] . '" style="border: 1px solid #EEEEEE;" /></a>';

// Text
$_['BACKEND_CH_PAYMENT']				= 'Zahlung';
$_['BACKEND_CH_SUCCESS']				= 'Erfolgreich : ' . $_['heading_title'] . ' ge&auml;ndert.';
$_['BACKEND_CH_EDIT']					= 'Bearbeiten ' . $_['heading_title'];

// Entry
$_['BACKEND_CH_SERVER']		    		= 'Server';
$_['BACKEND_CH_MODE_LIVE']				= 'LIVE';
$_['BACKEND_CH_MODE_TEST']				= 'TEST';
$_['BACKEND_CH_MODE']  					= 'Transaktions-Modus';
$_['BACKEND_CH_MODEDEBIT']				= 'Debit';
$_['BACKEND_CH_MODEPREAUTH']			= 'Pre-Authorization';
$_['BACKEND_CH_CARDS']					= 'Kartenarten';
$_['BACKEND_CH_AMOUNT']  				= 'Betrag für Registrierung';
$_['BACKEND_CH_MULTICHANNEL']  	  		= 'Multi-Kanal';
$_['BACKEND_CH_CHANNEL']  	  			= 'Entity-ID';
$_['BACKEND_CH_MOTO']  					= 'Entity-ID MOTO';
$_['BACKEND_CH_TOTAL']          		= 'Summe';
$_['BACKEND_CH_ZONE']					= 'Geo Zone';
$_['BACKEND_CH_STATUS']					= 'Status';
$_['BACKEND_CH_ORDER']        			= 'Reihenfolge';

// Help
$_['BACKEND_TT_REGISTRATION_AMOUNT']	= 'Betrag, der bei der Registrierung von Zahlungsarten gebucht und gutgeschrieben wird (wenn die Registrierung ohne Checkout erfolgt)';
$_['BACKEND_TT_CHANNEL_MOTO'] 			= 'Alternativer Kanal (z.B. ohne 3D Secure) für wiederkehrende Zahlungen (nur bei aktiviertem Multichannel benötigt)';
$_['BACKEND_TT_MULTICHANNEL'] 			= 'Wenn aktiviert, werden wiederkehrende Zahlungen über den alternativen Kanal abgewickelt';
$_['BACKEND_TT_TOTAL']         			= 'Der Warenkorb muss diese Summe beinhalten, damit dieses Zahlungsverfahren verf&uuml;gbar ist.';

// Error
$_['ERROR_PERMISSION']	      			= 'Warnung : Sie haben keine Berechtigung, um ' . $_['heading_title'] . ' zu &auml;ndern!';
