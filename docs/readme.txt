## additional instruction to show logo at checkout payment method
------------------------------------------------------------------

1. go to file {your shop root folder}/catalog/view/theme/default/template/checkout/payment_method.twig

change line 14:
	{{ payment_method.title }}

to this code:
	{% if payment_method.code|slice(0, 11) == 'genericshop' %}
        {{ payment_method.logo }}
    {% else %}
        {{ payment_method.title }}
    {% endif %}


## additional instruction for add menu my payment information at shopper account.
---------------------------------------------------------------------------------

1. go to file {your shop root folder}/catalog/controller/account/account.php

after line 68:
	$data['recurring'] = $this->url->link('account/recurring', '', true);

add this code:

	if($this->config->get('payment_genericshop_ageneral_recurring'))
	{
		$this->load->language('extension/payment/genericshop');
		$data['FRONTEND_MC_INFO'] = $this->language->get('FRONTEND_MC_INFO');
		$data['payment_information'] = $this->url->link('account/payment_information', '', true);
	}

2. go to file {your shop root folder}/catalog/view/theme/default/template/account/account.twig

after line 25:
	<li><a href="{{ wishlist }}">{{text_wishlist }}</a></li>

add this code:
	{% if payment_information is defined %}
      <li><a href="{{ payment_information }}">{{ FRONTEND_MC_INFO }}</a></li>
    {% endif %}

3. go to file {your shop root folder}/catalog/controller/extension/module/account.php

after line 23:
	$data['recurring'] = $this->url->link('account/recurring', '', true);

add this code:

	if($this->config->get('payment_genericshop_ageneral_recurring'))
	{
		$this->load->language('extension/payment/genericshop');
		$data['FRONTEND_MC_INFO'] = $this->language->get('FRONTEND_MC_INFO');
		$data['payment_information'] = $this->url->link('account/payment_information', '', true);
	}

4. go to file {your shop root folder}/catalog/view/theme/default/template/extension/module/account.twig

at line 9 after this code:
	<a href="{{ wishlist }}" class="list-group-item">{{text_wishlist }}</a>

add this code:
	{% if payment_information is defined %}
	  <a href="{{ payment_information }}" class="list-group-item">{{ FRONTEND_MC_INFO }}</a>
	{% endif %}


## additional instruction to add capture, refund and update status at backend order history.
--------------------------------------------------------------------------------------------

1. go to file {your shop root folder}/catalog/controller/api/order.php

at line 795:
	if ($order_info) {
		$this->model_checkout_order->addOrderHistory($order_id, $this->request->post['order_status_id'], $this->request->post['comment'], $this->request->post['notify'], $this->request->post['override']);

		$json['success'] = $this->language->get('text_success');
	} else {
		$json['error'] = $this->language->get('error_not_found');
	}

change to this code:
	if ($order_info) {
		if (substr($order_info['payment_code'],0,11) == 'genericshop')
		{
			$this->load->language('extension/payment/genericshop');
			$this->load->model('genericshop/genericshop');

			$order_status_id = $this->request->post['order_status_id'];
			$result = $this->model_genericshop_genericshop->doBackendPayment($order_id, $order_status_id);

			if ($result['status'])
			{
				$this->model_checkout_order->addOrderHistory($order_id, $result['order_status_id'], $this->request->post['comment'], $this->request->post['notify'], $this->request->post['override']);
				$json['success'] = $this->language->get($result['successMessage']);
			} else {
				$json['error'] = $this->language->get($result['errorMessage']);
			}
		}
		else
		{
			$this->model_checkout_order->addOrderHistory($order_id, $this->request->post['order_status_id'], $this->request->post['comment'], $this->request->post['notify'], $this->request->post['override']);

			$json['success'] = $this->language->get('text_success');
		}
	} else {
		$json['error'] = $this->language->get('error_not_found');
	}


## additional instruction to add recurring payment at backend add order.
------------------------------------------------------------------------

1. go to file {your shop root folder}/catalog/controller/api/order.php

at line 352
	$this->model_checkout_order->addOrderHistory($json['order_id'], $order_status_id);

change to this code
	if (isset($this->request->post['recurring_id']))
	{
		$this->load->model('genericshop/genericshop');
		$recurring_id = $this->request->post['recurring_id'];
		$backendOrderResult = $this->model_genericshop_genericshop->createBackendOrder($json['order_id'], $recurring_id);
        if (!$backendOrderResult['status'] && !empty($backendOrderResult['errorMessage'])) {
            unset($json['success']);
            $json['error'] = $this->language->get($backendOrderResult['errorMessage']);
        } else {
            $this->model_checkout_order->addOrderHistory($json['order_id'], $backendOrderResult['order_status_id'], $backendOrderResult['comment']);
        }
	}
	else
	{
		$this->model_checkout_order->addOrderHistory($json['order_id'], $order_status_id);
	}

at line 691 :
    $this->model_checkout_order->editOrder($order_id, $order_data);

    // Set the order history
    if (isset($this->request->post['order_status_id'])) {
        $order_status_id = $this->request->post['order_status_id'];
    } else {
        $order_status_id = $this->config->get('config_order_status_id');
    }

    $this->model_checkout_order->addOrderHistory($order_id, $order_status_id);

change to this code:
    if (strpos($order_data['payment_code'], "genericshop") !== false) {
        $this->load->model('genericshop/genericshop');
        if(isset($this->request->post['recurring_id'])){
            $recurring_id = $this->request->post['recurring_id'];
            $backendOrderResult = $this->model_genericshop_genericshop->createBackendOrder($order_id,$recurring_id);
            if (!$backendOrderResult['status'] && !empty($backendOrderResult['errorMessage'])) {
                unset($json['success']);
                $json['error'] = $this->language->get($backendOrderResult['errorMessage']);
            }
        }
    }
    if(isset($json['success'])) {
        $this->model_checkout_order->editOrder($order_id, $order_data);

        // Set the order history
        if (isset($this->request->post['order_status_id'])) {
            $order_status_id = $this->request->post['order_status_id'];
        } else {
            $order_status_id = $this->config->get('config_order_status_id');
        }

        $this->model_checkout_order->addOrderHistory($order_id, $order_status_id);
    }

2. go to file {your shop root folder}/catalog/controller/api/payment.php

after line 202
	$this->load->model('extension/payment/' . $result['code']);

add this code

	if (substr($result['code'],0,11) == 'genericshop' && $this->{'model_extension_payment_' . $result['code']}->isNotAllowedPaymentBackend())
		continue;

before the end of file
	}

add this code

	public function recurring() {
		$this->load->language('api/payment');

		$json = array();

		if (!isset($this->session->data['api_id'])) {
			$json['error'] = $this->language->get('error_permission');
		} else {
			// Payment Address
			if (!isset($this->session->data['payment_address'])) {
				$json['error'] = $this->language->get('error_address');
			}

			if (!isset($this->request->post['payment_method'])) {
				$json['error'] = $this->language->get('error_method');
			}

			if (!$json) {
				$paymentMethod = $this->request->post['payment_method'];
				$this->session->data['payment_method'] = $paymentMethod;

				$this->load->language('extension/payment/genericshop');
				$this->load->model('extension/payment/' . $paymentMethod);
				$json['recurring'] = $this->{'model_extension_payment_' . $paymentMethod}->getBackendRecurringLists();
				$json['ending'] = $this->language->get('FRONTEND_MC_ENDING');
				$json['validity'] = $this->language->get('FRONTEND_MC_VALIDITY');
				$json['account'] = $this->language->get('FRONTEND_MC_ACCOUNT');
				$json['email'] = $this->language->get('FRONTEND_MC_EMAIL');
				$json['success'] = $this->language->get('text_method');
			}
		}

		if (isset($this->request->server['HTTP_ORIGIN'])) {
			$this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
			$this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
			$this->response->addHeader('Access-Control-Max-Age: 1000');
			$this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}


3. go to file {your shop root folder}/admin/view/template/sale/order_form.twig

before line 870
	<div class="form-group">
      <label class="col-sm-2 control-label" for="input-coupon"><?php echo $entry_coupon; ?></label>

add this code
    <div class="form-group genericshop_cc_saved" style="display:none">
      <label class="col-sm-2 control-label"></label>
      <div class="col-sm-10">
        <div id="input-group" class="genericshop_cc_saved-selected">
        </div>
      </div>
    </div>
    <div class="form-group genericshop_dd_saved" style="display:none">
      <label class="col-sm-2 control-label"></label>
      <div class="col-sm-10">
        <div id="input-group" class="genericshop_dd_saved-selected">
        </div>
      </div>
    </div>

before line 1995
	$('#button-payment-method').on('click', function() {

add this code
	$('#input-payment-method').on('change', function() {
	  $('.genericshop_cc_saved').hide();
	  $('.genericshop_cc_saved-selected').html('');

	  var paymentMethod = $('select[name=\'payment_method\'] option:selected').val();
	  if (paymentMethod.substring(0, 11) == 'genericshop')
	  {
	    $.ajax({
	      url: '{{ store_url }}index.php?route=api/payment/recurring&api_token={{ api_token }}',
	      type: 'post',
	      data: 'payment_method=' + $('select[name=\'payment_method\'] option:selected').val() + '&customer_id=' + $('#tab-customer input[name=\'customer_id\']').val(),
	      dataType: 'json',
	      crossDomain: true,
	      beforeSend: function() {
	        $('#button-payment-method').button('loading');
	      },
	      complete: function() {
	        $('#button-payment-method').button('reset');
	      },
	      success: function(json) {
	        $('.alert, .text-danger').remove();
	        $('.form-group').removeClass('has-error');

	        if (json['error']) {
	          $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

	          // Highlight any found errors
	          $('select[name=\'payment_method\']').parent().parent().parent().addClass('has-error');
	        }

	        if (json['success']) {
	          var _html = '';
	          jQuery.each(json['recurring'], function(key, val) {
	            var checked = '';
	            if (val['payment_default'] == 1){
	              checked = 'checked="checked"';
	            }
	            if (paymentMethod == 'genericshop_cc_saved'){
	              brand = '<img src="view/image/payment/genericshop/'+val['brand'].toLowerCase()+'.png" height="35px">';
	              info = json['ending']+' '+val['last4digits']+' '+json['validity']+' '+val['expiry_month']+'/'+val['expiry_year'].substring(2);
	            }

	            _html += '<div class="radio"><label><input type="radio" id="recurring_id" name="recurring_id" value="'+val['id']+'" '+checked+' />'+brand+' '+info+'</label></div>';
	          });
	          $('.' + paymentMethod + '-selected').html(_html);

	          // Refresh products, vouchers and totals
	          $('#button-refresh').trigger('click');
	        }
	      },
	      error: function(xhr, ajaxOptions, thrownError) {
	        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
	      }
	    });
	    $('.'+paymentMethod).show();
	  }
	});

at line 2251
	data: $('select[name=\'payment_method\'] option:selected,  select[name=\'shipping_method\'] option:selected,  #tab-total select[name=\'order_status_id\'], #tab-total select, #tab-total textarea[name=\'comment\'], #tab-total input[name=\'affiliate_id\']'),

change to this code
	data: $('select[name=\'payment_method\'] option:selected,  #recurring_id, select[name=\'shipping_method\'] option:selected,  #tab-total select[name=\'order_status_id\'], #tab-total select, #tab-total textarea[name=\'comment\'], #tab-total input[name=\'affiliate_id\']'),


#############################################################################

How to add Gender to account :
------------------------------

1. go to opencart admin
2. select menu Customers - Custom Fields
3. Input :
	Custom Field Name : Gender
	Location : Account
	Type : Radio
	Customer Group : Default
	Required : -
	Status : Enabled
	Sort Order : 0
	Custom Field Value name :
		Mr, Sort Order 1
		Mrs, Sort Order 2
4. Save

How to add BirthDate to account :
---------------------------------

1. go to opencart admin
2. select menu Customers - Custom Fields
3. Input :
	Custom Field Name : Birth Date
	Location : Account
	Type : Date
	Value : -
	Validation : -
	Customer Group : Default
	Required : -
	Status : Enabled
	Sort Order : 3
4. Save